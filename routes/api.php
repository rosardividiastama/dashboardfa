<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/postlender',[
    'uses' => 'api\apiPostLenderController@store',
    'as' => 'api.apiPostLenderController.store',
    'middleware' => 'cors'
    ]);

Route::post('/postborrower',[
    'uses' => 'api\apiPostBorrowerController@store',
    'as' => 'api.apiPostBorrowerController.store',
    'middleware' => 'cors'
    ]);

Route::post('/lender/up/importfile',[
    'uses' => 'api\apiPostLenderController@importfile',
    'as' => 'api\apiPostLenderController.importfile',
    'middleware' => 'cors'
    ]);

Route::post('/borrower/up/importfile',[
    'uses' => 'api\apiPostBorrowerController@importfile',
    'as' => 'api\apiPostBorrowerController.importfile',
    'middleware' => 'cors'
    ]);
