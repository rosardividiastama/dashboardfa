<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes([
    'reset' => false,
    'register'=>false
    ]);

Route::post('/validation/jwt',[
    'uses'=>'AuthController@authenticate',
    'middleware'=>['cors'],
]);

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home',[
    'uses' => 'HomeController@index',
    'as' => 'HomeController.index'
    ]);

Route::post('/home',[
    'uses' => 'HomeController@store',
    'as' => 'HomeController.store'
    ]);

Route::get('/bulan',[
    'uses' => 'BulanController@index',
    'as' => 'BulanController.index'
    ]);

Route::get('/akum',[
    'uses' => 'AkumulasiController@index',
    'as' => 'AkumulasiController.index'
    ]);

// ========== admin area ==========
Route::get('/adminarea',[
    'uses' => 'AdminController@index',
    'as' => 'AdminController.index'
    ]);

Route::get('/admin_pendanaan',[
    'uses' => 'PendanaanController@index',
    'as' => 'PendanaanController.index'
    ]);

    Route::get('/pendanaan/json',[
        'uses' => 'PendanaanController@index_json',
        'as' => 'PendanaanController.index_json'
        ]);

    Route::post('/input_pendanaan',[
        'uses' => 'PendanaanController@store',
        'as' => 'PendanaanController.store'
        ]);

    Route::get('/edit_pendanaan/{id}/edit',[
        'uses' => 'PendanaanController@edit',
        'as' => 'PendanaanController.edit'
        ]);
    
    Route::post('/update_pendanaan/{id}',[
        'uses' => 'PendanaanController@update',
        'as' => 'PendanaanController.update'
        ]);
    
Route::get('/admin_outstanding',[
    'uses' => 'OutstandingController@index',
    'as' => 'OutstandingController.index'
    ]);

    Route::get('/outstanding_json',[
        'uses' => 'OutstandingController@index_json',
        'as' => 'OutstandingController.index_json'
        ]);

    Route::post('/input_outstanding',[
        'uses' => 'OutstandingController@store',
        'as' => 'OutstandingController.store'
        ]);

    Route::get('/edit_outstanding/{id}/edit',[
        'uses' => 'OutstandingController@edit',
        'as' => 'OutstandingController.edit'
        ]);
    
    Route::post('/update_outstanding/{id}',[
        'uses' => 'OutstandingController@update',
        'as' => 'OutstandingController.update'
        ]);
    

Route::get('/admin_penempatan_dana',[
    'uses' => 'PenempatanDanaController@index',
    'as' => 'PenempatanDanaController.index'
    ]);

    Route::get('/penempatan_json',[
        'uses' => 'PenempatanDanaController@index_json',
        'as' => 'PenempatanDanaController.index_json'
        ]);

    Route::post('/input_penempatan',[
        'uses' => 'PenempatanDanaController@store',
        'as' => 'PenempatanDanaController.store'
        ]);

    Route::get('/edit_penempatan/{id}/edit',[
        'uses' => 'PenempatanDanaController@edit',
        'as' => 'PenempatanDanaController.edit'
        ]);
    
    Route::post('/update_penempatan/{id}',[
        'uses' => 'PenempatanDanaController@update',
        'as' => 'PenempatanDanaController.update'
        ]);

Route::get('/admin_withdraw',[
    'uses' => 'WithdrawController@index',
    'as' => 'WithdrawController.index'
    ]);

    Route::get('/withdraw_json',[
        'uses' => 'WithdrawController@index_json',
        'as' => 'WithdrawController.index_json'
        ]);

    Route::post('/input_withdraw',[
        'uses' => 'WithdrawController@store',
        'as' => 'WithdrawController.store'
        ]);

    Route::get('/edit_withdraw/{id}/edit',[
        'uses' => 'WithdrawController@edit',
        'as' => 'WithdrawController.edit'
        ]);
    
    Route::post('/update_withdraw/{id}',[
        'uses' => 'WithdrawController@update',
        'as' => 'WithdrawController.update'
        ]);

Route::get('/maps',[
    'uses' => 'MapsController@index',
    'as' => 'MapsController.index'
    ]);
