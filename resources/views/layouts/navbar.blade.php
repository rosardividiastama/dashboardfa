<!-- partial:partials/_navbar.html -->
<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
	<div class="text-center navbar-full-wrapper d-flex align-items-top justify-content-center">
	  {{-- <a class="navbar-brand brand-logo" href="index.html">
		<img src="images/logo.svg" alt="logo" />
	  </a> --}}
		<a class="navbar-brand brand-logo-mini" href="index.html">
			{{-- <img src="images/logo_danain.png" alt="logo" /> --}}
		  </a>
	  
	</div>
	<div class="navbar-menu-wrapper d-flex align-items-center">
	  <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
		{{-- <li class="nav-item">
		  <a href="#" class="nav-link">Schedule
			<span class="badge badge-primary ml-1">New</span>
		  </a>
		</li> --}}
		<li class="nav-item active">
		  <a href="/home" class="nav-link">
			<i class="mdi mdi-elevation-rise"></i>DATA TAHUN BERJALAN</a>
		</li>
		<li class="nav-item">
		  <a href="/bulan" class="nav-link">
			<i class="mdi mdi-bookmark-plus-outline"></i>DATA BULAN BERJALAN</a>
		</li>
		<li class="nav-item">
			<a href="/akum" class="nav-link">
			  <i class="mdi mdi-bookmark-plus-outline"></i>DATA AKUMULASI</a>
		</li>
		<li class="nav-item">
			<a href="/maps" class="nav-link">
			  <i class="mdi mdi-bookmark-plus-outline"></i>MAPS</a>
		</li>
	  </ul>
	  <ul class="navbar-nav navbar-nav-right">
		<li>
			<form>
				{{ csrf_field() }}
				{{-- <label class="control-label"></label> --}}
				{{-- <input id="datepicker" type="text" ej-datepicker class="form-control" style="color: white" name="datepicker" onchange="datepickup()"> --}}
				<input type="month" id="datepicker" name="datepicker" onchange="datepickup()">
			</form>
		</li>
		{{-- <li style="margin-right: 15px;margin-left:15px;">
			
			<a class="nav-link profile-text"  href="{{ route('logout') }}" 
			onclick="event.preventDefault();
			document.getElementById('logout-form').submit();" style="color: white">
			{{ __('Logout') }}</a>
		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			{{ csrf_field() }}
		</form>
	</li> --}}
		<li class="nav-item dropdown d-none d-xl-inline-block">
		  <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
			<span class="profile-text">Admin</span>
			{{-- <img class="img-xs rounded-circle" src="images/faces/face1.jpg" alt="Profile image"> --}}
		  </a>
		  
		  <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
			<a class="dropdown-item p-0">
			  {{-- <div class="d-flex border-bottom">
				<div class="py-3 px-4 d-flex align-items-center justify-content-center">
				  <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
				</div>
				<div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
				  <i class="mdi mdi-account-outline mr-0 text-gray"></i>
				</div>
				<div class="py-3 px-4 d-flex align-items-center justify-content-center">
				  <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
				</div>
			  </div> --}}
			</a>
			<a class="dropdown-item mt-2" href="{{ route('AdminController.index') }}" >
			  Admin Area
			</a>
			<a class="dropdown-item"  href="{{ route('logout') }}" 
			onclick="event.preventDefault();
			document.getElementById('logout-form').submit();" style="color: black">
			{{ __('Logout') }}</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
			</form>
		  </div>
		</li>
	  </ul>
	  <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
		<span class="mdi mdi-menu"></span>
	  </button>
	</div>
  </nav>

@section('extracss')
@endsection

@section('extrajs')
<script>
	function datepickup() {
		var x = document.getElementById("datepicker").value;
	  console.log(x)
	}
</script>
@endsection