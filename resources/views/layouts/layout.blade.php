<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap  -->
	<link href="{{ asset('asset/css/bootstrap.css') }}" rel="stylesheet">
	{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
	
	{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> --}}
  
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <!-- plugins:css -->
	<link rel="stylesheet" href="{{ asset('asset/iconfonts/mdi/css/materialdesignicons.min.css') }}">
	<link rel="stylesheet" href="{{ asset('asset/css/vendor.bundle.base.css') }}">
	<link rel="stylesheet" href="{{ asset('asset/css/vendor.bundle.addons.css') }}">

	<!-- inject:css -->
	<link rel="stylesheet" href="{{ asset('asset/css/style.css') }}">

	<link rel="shortcut icon" href="{{ asset('asset/images/favicon.png') }}" />
	<link rel="shortcut icon" href="asset/images/favicon.png" />
	<link rel="stylesheet" href="asset/css/material-datepicker.css">

	<title>@yield('tittle')</title>
	@yield('extracss')
</head>
<body>
	<div class="container-scroller">
		@include('layouts.navbar')
		<div class="container-fluid">
			{{-- @include('layouts.sidebar') --}}
				@yield('content')
		</div>
	</div>

	<!-- plugins:js -->
	<script src="asset/js/vendor.bundle.base.js"></script>
	<script src="asset/js/vendor.bundle.addons.js"></script>
	<script src="asset/js/off-canvas.js"></script>
	<script src="asset/js/misc.js"></script>
	<script src="asset/js/dashboard.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
	<script  src="/asset/js/ej2-datepicker.min.js"></script>

	{{-- datepicker js --}}

{{-- <script>
    var datepicker = new ej.calendars.DatePicker({ 
        width: "200px",
        placeholder: 'Choose a date',
		format: 'MM-yyyy',
        value: new Date()
        
        });
    datepicker.appendTo('#datepicker');
	
</script>--}}
<script>
	function datepickup() {
		var month = document.getElementById("datepicker").value;
	  	console.log(month);
		var url = window.location.href.split('/');
		var lastSegment = url.pop() || url.pop();
		console.log("/"+lastSegment);
		
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		  $.ajax({
				_token: CSRF_TOKEN,
				type: "POST",
				url: "/"+lastSegment,
				dataType: 'JSON',
				data: { segment: lastSegment, date: month }
			});
	}
</script>

	@yield('extrajs')

	
</body>
</html>