<nav class="sidebar sidebar-offcanvas" id="sidebar">
	<ul class="nav">
	  <li class="nav-item nav-profile">
	  </li>
	  <li class="nav-item">
		<a class="nav-link" href="{{ route('AdminController.index') }}">
		  <i class="menu-icon mdi mdi-account"></i>
		  <span class="menu-title">User Account</span>
		</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" href="{{ route('PendanaanController.index') }}">
		  <i class="menu-icon mdi mdi-currency-usd"></i>
		  <span class="menu-title">Pendanaan / Lending</span>
		</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" href="{{ route('OutstandingController.index') }}">
		  <i class="menu-icon mdi mdi-reload"></i>
		  <span class="menu-title">Outstanding</span>
		</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" href="{{ route('PenempatanDanaController.index') }}">
		  <i class="menu-icon mdi mdi-chart-line"></i>
		  <span class="menu-title">Penempatan Dana</span>
		</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" href="{{ route('WithdrawController.index') }}">
		  <i class="menu-icon mdi mdi-chart-line"></i>
		  <span class="menu-title">Withdraw</span>
		</a>
	  </li>
	  
	  <li class="nav-item">
		<a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
		  <i class="menu-icon mdi mdi-account-multiple"></i>
		  <span class="menu-title">Customer</span>
		  <i class="menu-arrow"></i>
		</a>
		<div class="collapse" id="ui-basic">
		  <ul class="nav flex-column sub-menu">
			<li class="nav-item">
			  <a class="nav-link" href="">Lender</a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="">Borrower</a>
			</li>
		  </ul>
		</div>
	  </li>
	  
	  
	  
	  {{-- <li class="nav-item">
		<a class="nav-link" href="pages/icons/font-awesome.html">
		  <i class="menu-icon mdi mdi-sticker"></i>
		  <span class="menu-title">Icons</span>
		</a>
	  </li> --}}
	  {{-- <li class="nav-item">
		<a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
		  <i class="menu-icon mdi mdi-restart"></i>
		  <span class="menu-title">User Pages</span>
		  <i class="menu-arrow"></i>
		</a>
		<div class="collapse" id="auth">
		  <ul class="nav flex-column sub-menu">
			<li class="nav-item">
			  <a class="nav-link" href="pages/samples/blank-page.html"> Blank Page </a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="pages/samples/login.html"> Login </a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="pages/samples/register.html"> Register </a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="pages/samples/error-404.html"> 404 </a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="pages/samples/error-500.html"> 500 </a>
			</li>
		  </ul>
		</div>
	  </li> --}}
	</ul>
  </nav>