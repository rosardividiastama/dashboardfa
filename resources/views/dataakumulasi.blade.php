@extends('layouts.layout')

@section('tittle')
Beranda
@endsection

@section('content')
    <div class="container-scroller">
        <div class="container-fluid">
          <div class="full-panel">
              <div class="content-wrapper">
                {{-- statistics --}}
                <div class="row">
                  <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                    <div class="card card-statistics">
                      <div class="card-body">
                          <h4 class="mb-0 text-right">Penyaluran</h4>
                        <div class="clearfix">
                          <div class="float-left">
                            <i class="mdi mdi-cube text-danger icon-md"></i>
                          </div>
                          <div class="float-right">
                            <h5 class="mb-0 text-right">Pinjaman</h5>
                            <div class="fluid-container">
                              <h3 id="lending" class="font-weight-medium text-right mb-0"></h3>
                            </div>
                            <h5 class="mb-0 text-right">Milyar</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                      <div class="card card-statistics">
                        <div class="card-body">
                          <h4 class="mb-0 text-right">Pendana</h4>
                          <div class="clearfix">
                            <div class="float-left">
                              <i class="mdi mdi-account-location text-info icon-md"></i>
                            </div>
                            <div class="float-right">
                              <h5 class="mb-0 text-right">Terdatar</h5>
                              <div class="fluid-container">
                                <h3 id="pendana" class="font-weight-medium text-right mb-0"></h3>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                      <div class="card card-statistics">
                      <div class="card-body">
                          <h4 class="mb-0 text-right">Peminjam</h4>
                          <div class="clearfix">
                          <div class="float-left">
                              <i class="mdi mdi-account-location text-info icon-md"></i>
                          </div>
                          <div class="float-right">
                              <h5 class="mb-0 text-right">Employees</h5>
                              <div class="fluid-container">
                              <h3 id="peminjam" class="font-weight-medium text-right mb-0"></h3>
                              </div>
                          </div>
                          </div>
                      </div>
                      </div>
                  </div>
                  <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                    <div class="card card-statistics">
                      <div class="card-body">
                          <h4 class="mb-0 text-right">Outstanding</h4>
                        <div class="clearfix">
                          <div class="float-left">
                            <i class="mdi mdi-receipt text-warning icon-md"></i>
                          </div>
                          <div class="float-right">
                            <h5 class="mb-0 text-right">Pinjaman</h5>
                            <div class="fluid-container">
                              <h3 id="outstanding" class="font-weight-medium text-right mb-0"></h3>
                            </div>
                            <h5 class="mb-0 text-right">Milyar</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                    <div class="card card-statistics">
                      <div class="card-body">
                          <h4 class="mb-0 text-right">TKB</h4>
                        <div class="clearfix">
                          <div class="float-left">
                            <i class="mdi mdi-poll-box text-success icon-md"></i>
                          </div>
                          <div class="float-right">
                            <h5 class="mb-0 text-right">90</h5>
                            <div class="fluid-container">
                              <h3 class="font-weight-medium text-right mb-0">100 %</h3>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- pie diagram --}}
                  <div class="row">
                      <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                          <div class="card card-body">
                              <h4 class="card-title">Tujuan Pinjaman %</h4>
                              <canvas id="pieChart1" style="height:150px"></canvas>
                          </div>
                      </div>
                      <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                        <div class="card card-body">
                              <h4 class="card-title">Pendana %</h4>
                              <canvas id="pieChart2" style="height:150px"></canvas>
                          </div>
                      </div>
                      <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                          <div class="card card-body">
                              <h4 class="card-title">Peminjam %</h4>
                              <canvas id="pieChart3" style="height:150px"></canvas>
                          </div>
                      </div>
                      <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                          <div class="card card-body">
                              <h4 class="card-title">O/S Pinjaman %</h4>
                              <canvas id="pieChart4" style="height:150px"></canvas>
                          </div>
                      </div>
                      <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                          <div class="card card-body">
                              <h4 class="card-title">Pinjaman Lunas %</h4>
                              <canvas id="pieChart5" style="height:150px"></canvas>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 grid-margin stretch-card">
                      <div class="card">
                          <div class="card-body">
                          <h4 class="card-title">PENDANA</h4>
                          <canvas id="hbarChart1" style="height:250px"></canvas>
                          </div>
                      </div>
                    </div>
                      <div class="col-lg-3 grid-margin stretch-card">
                          <div class="card">
                              <div class="card-body">
                              <h4 class="card-title">PEMINJAM</h4>
                              <canvas id="hbarChart2" style="height:250px"></canvas>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 grid-margin stretch-card">
                          <div class="card">
                              <div class="card-body">
                              <h4 class="card-title">TICKET SIZE %</h4>
                              <canvas id="hbarChart3" style="height:250px"></canvas>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 grid-margin stretch-card">
                          <div class="card">
                              <div class="card-body">
                              <h4 class="card-title">OUTSTANDING %</h4>
                              <canvas id="hbarChart4" style="height:250px"></canvas>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- content-wrapper ends -->
              <!-- partial:../../partials/_footer.html -->
              <footer class="footer">
              <div class="container-fluid clearfix">
                  <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                  <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
                  <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                  <i class="mdi mdi-heart text-danger"></i>
                  </span>
              </div>
              </footer>
              <!-- partial -->
          </div>
        <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@endsection

@section('extracss')
    <style>
        /* Style the tab */
        .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
        }
        
        /* Style the buttons inside the tab */
        .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
        }
        
        /* Change background color of buttons on hover */
        .tab button:hover {
        background-color: #ddd;
        }
        
        /* Create an active/current tablink class */
        .tab button.active {
        background-color: #ccc;
        }
        
        /* Style the tab content */
        .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        }

        .gallery{
            display: inline-block;
            margin-top: 20px;
        }
        
        .close-icon{
            border-radius: 50%;
            position: absolute;
            right: 5px;
            top: -10px;
            padding: 5px 8px;
        }
        .form-image-upload{
            background: #e8e8e8 none repeat scroll 0 0;
            padding: 15px;
        }
        .img-responsive{
            max-width: 100%;
        }
        </style>
@endsection

@section('extrajs')
{{-- <script src="js/chart.js"></script> --}}
<script>
    
    var lending = @json($lending);
    obj = JSON.parse(lending);
    lending = ((obj.sum_untilnow)/1000000000).toFixed(2);
    pendana = obj.pendana_untilnow;
    peminjam = obj.peminjam_untilnow;
    os_untilnow = ((obj.os_untilnow)/1000000000).toFixed(2);
    pendana_female = ((obj.pendana_female)*100).toFixed(2);
    pendana_male = ((obj.pendana_male)*100).toFixed(2);
    peminjam_female = ((obj.peminjam_female)*100).toFixed(2);
    peminjam_male = ((obj.peminjam_male)*100).toFixed(2);

    os_untilnow_individu = ((obj.os_untilnow_individu)*100).toFixed(2);
    os_untilnow_captive = ((obj.os_untilnow_captive)*100).toFixed(2);
    os_untilnow_institusi = ((obj.os_untilnow_institusi)*100).toFixed(2);

    document.getElementById("lending").innerHTML = lending;
    document.getElementById("pendana").innerHTML = pendana;
    document.getElementById("peminjam").innerHTML = peminjam;
    document.getElementById("outstanding").innerHTML = os_untilnow;

    var agependana = @json($agespendana);
    ages = JSON.parse(agependana);
    t24an = ages.t24an;
    t25an = ages.t25an;
    t35an = ages.t35an;
    t55an = ages.t55an;

    b24an = ages.b24an;
    b25an = ages.b25an;
    b35an = ages.b35an;
    b55an = ages.b55an;
    
    console.log(os_untilnow_institusi, os_untilnow_individu,os_untilnow_captive);

    $(function() {
  /* ChartJS
   * -------
   * Data and config for chartjs
   */
  'use strict';
  var datapendana = {
    labels: [">55thn", "35-55thn", "25-34thn","<24thn"],
    datasets: [{
      // label: 'MTD',
      data: [t55an, t35an,t25an,t24an],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var datapeminjam = {
    labels: [">55thn", "35-55thn", "25-34thn","<24thn"],
    datasets: [{
      // label: 'MTD',
      data: [b55an, b35an,b25an,b24an],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var dataticket = {
    labels: [">30jt", "15-30jt", "5-15jt","<1jt"],
    datasets: [{
      // label: 'MTD',
      data: [3, 8,21,43,25],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var dataos = {
    labels: ["institusi", "individu", "captive"],
    datasets: [{
      // label: 'MTD',
      data: [os_untilnow_institusi, os_untilnow_individu,os_untilnow_captive],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var options = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false
    },
    elements: {
      point: {
        radius: 0
    },
    labels: {
      render: 'value'
    }
    }

  };

  var doughnutPieData = {
    datasets: [{
      data: [10, 90],
      backgroundColor: [
        'rgba(137, 201, 184, 0.5)',
        'rgba(255, 163, 108, 0.5)',
      ],
      borderColor: [
        'rgba(137, 201, 184,1)',
        'rgba(255, 163, 108, 1)',
      ],
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    // labels: [
    //   'Pink',
    //   'Blue',
    //   'Yellow',
    // ]
  };
  var doughnutPieOptions = {
    responsive: true,
    animation: {
      animateScale: true,
      animateRotate: true
    },
    legend: {
       display: true,
       align:'start',
       position: 'bottom'
      },
      labels: {
      render: 'value'
    }
  };

  var genderPendanaPieData = {
    datasets: [{
      data: [pendana_female, pendana_male],
      backgroundColor: [
        'rgba(255, 99, 132, 0.5)',
        'rgba(54, 162, 235, 0.5)',
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
      ],
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
      'Female',
      'Male'
    ]
  };

  var genderPeminjamPieData = {
    datasets: [{
      data: [peminjam_female, peminjam_male],
      backgroundColor: [
        'rgba(255, 99, 132, 0.5)',
        'rgba(54, 162, 235, 0.5)',
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
      ],
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
      'Female',
      'Male'
    ]
  };

  var provinsiPieData = {
    datasets: [{
      data: [40, 60],
      backgroundColor: [
        'rgba(255, 206, 86, 0.5)',
        'rgba(75, 192, 192, 0.5)',
      ],
      borderColor: [
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
      ],
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
      'Jawa',
      'Luar Jawa'
    ]
  };
  
  // Get context with jQuery - using jQuery's .get() method.
  if ($("#barChart1").length) {
    var barChartCanvas = $("#barChart1").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: dataytd,
      options: options
    });
  }

  if ($("#doughnutChart").length) {
    var doughnutChartCanvas = $("#doughnutChart").get(0).getContext("2d");
    var doughnutChart = new Chart(doughnutChartCanvas, {
      type: 'doughnut',
      data: doughnutPieData,
      options: doughnutPieOptions
    });
  }

  if ($("#pieChart1").length) {
    var pieChartCanvas1 = $("#pieChart1").get(0).getContext("2d");
    var pieChart1 = new Chart(pieChartCanvas1, {
      type: 'pie',
      data: doughnutPieData,
      options: doughnutPieOptions
    });
  }

  if ($("#pieChart2").length) {
    var pieChartCanvas2 = $("#pieChart2").get(0).getContext("2d");
    var pieChart2 = new Chart(pieChartCanvas2, {
      type: 'pie',
      data: genderPendanaPieData,
      options: doughnutPieOptions
    });
  }

  if ($("#pieChart3").length) {
    var pieChartCanvas3 = $("#pieChart3").get(0).getContext("2d");
    var pieChart3 = new Chart(pieChartCanvas3, {
      type: 'pie',
      data: genderPeminjamPieData,
      options: doughnutPieOptions
    });
  }

  if ($("#pieChart4").length) {
    var pieChartCanvas4 = $("#pieChart4").get(0).getContext("2d");
    var pieChart4 = new Chart(pieChartCanvas4, {
      type: 'pie',
      data: provinsiPieData,
      options: doughnutPieOptions
    });
  }

  if ($("#pieChart5").length) {
    var pieChartCanvas5 = $("#pieChart5").get(0).getContext("2d");
    var pieChart5 = new Chart(pieChartCanvas5, {
      type: 'pie',
      data: doughnutPieData,
      options: doughnutPieOptions
    });
  }

  if ($("#hbarChart1").length) {
    var hbarChartCanvas1 = $("#hbarChart1").get(0).getContext("2d");
    var hbarChart1 = new Chart(hbarChartCanvas1, {
      type: 'horizontalBar',
      data: datapendana,
      options: options
    });
  }
  
  if ($("#hbarChart2").length) {
    var hbarChartCanvas2 = $("#hbarChart2").get(0).getContext("2d");
    var hbarChart2 = new Chart(hbarChartCanvas2, {
      type: 'horizontalBar',
      data: datapeminjam,
      options: options
    });
  }
  
  if ($("#hbarChart3").length) {
    var hbarChartCanvas3 = $("#hbarChart3").get(0).getContext("2d");
    var hbarChart3 = new Chart(hbarChartCanvas3, {
      type: 'horizontalBar',
      data: dataticket,
      options: options
    });
  }

  if ($("#hbarChart4").length) {
    var hbarChartCanvas4 = $("#hbarChart4").get(0).getContext("2d");
    var hbarChart4 = new Chart(hbarChartCanvas4, {
      type: 'horizontalBar',
      data: dataos,
      options: options
    });
  }

});


</script>
@endsection
