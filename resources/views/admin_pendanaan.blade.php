@extends('layouts.adminlayout')

@section('tittle')
Beranda
@endsection

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
{{-- input baru --}}
      <div class="row">
        <div class="col-md-12 grid-margin">
          <div class="card">
                @if(session('isSukses')=='1')
                <div class="alert alert-success">
                    <button type="button" aria-label="Close" data-dismiss="alert" class="close">
                        <i class="menu-icon mdi mdi-close"></i>
                    </button>
                    <span aria-hidden="true">
                    <b> Success - {{ session('message') }}</b></span>
                </div>
                @elseif(session('isSukses')=='2')
                <div class="alert alert-danger">
                    <button type="button" aria-label="Close" data-dismiss="alert" class="close" >
                        <i class="menu-icon mdi mdi-close"></i>
                    </button>
                    <span>
                    <b> Error - {{ session('message') }}</b></span>
                </div>
                @endif
            <div class="card-body">
                <h3 class="text-primary">Input Pendanaan Bulan Baru</h3>
                <form method="post" action="{{ route('PendanaanController.store') }}">
                    {{ csrf_field() }}
                    <div class="row d-none d-sm-flex mb-4">
                        <div class="col-2">
                            <h5 class="text-primary">Bulan Tahun</h5>
                            <input type="date" id="date" name="date" class="form-control form-control-sm">
                        </div>
                        <div class="col-2">
                            <h5 class="text-primary">Type</h5>
                            <select name="type" id="type" class="form-control form-control-sm">
                                <option value="Individu">Individu</option>
                                <option value="Captive">Captive</option>
                                <option value="Institusi">Institusi</option>
                            </select>
                        </div>
                        <div class="col-3">
                            <h5 class="text-primary">Nominal</h5>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input id="nominal" name="nominal" type="number" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-3">
                            <h5 class="text-primary">Target</h5>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input id="target" name="target" type="number" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-2">
                            <h5 class="text-primary">&nbsp;</h5>&nbsp;
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
{{-- table grid --}}
      <div class="row">
        <div class="col-md-12 grid-margin">
          <div class="card">
            <div class="card-body">
                <h3 class="text-primary">Daftar Pendanaan</h3>
                <div class="card-body">
                    <div class="table-responsive">
                      <table id="users-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Type</th>
                                <th>Periode</th>
                                <th>Nominal</th>
                                <th>Target</th>
                                <th>Action</th>
                            </tr>                        
                        </thead>
                      </table>
                    </div>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="productForm" name="productForm" class="form-horizontal">
                        {{ csrf_field() }}
                       <input type="hidden" name="mlending_id" id="mlending_id" value="">
                        <div class="col-8">
                            <h5 class="text-primary">Nominal</h5>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input id="mnominal" name="mnominal" type="number" class="form-control form-control-sm" value="">
                            </div>
                        </div>
                        <div class="col-8">
                            <h5 class="text-primary">Target</h5>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input id="mtarget" name="mtarget" type="number" class="form-control form-control-sm" value="">
                            </div>
                        </div>
                        <p></p>
          
                        <div class="col-2">
                         <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                         </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- partial:../../partials/_footer.html -->
    
    <footer class="footer">
    <div class="container-fluid clearfix">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
        <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
        <i class="mdi mdi-heart text-danger"></i>
        </span>
    </div>
    </footer>
    <!-- partial -->
</div>
@endsection

@section('extracss')
    <style>
        /* Style the tab */
        .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
        }
        
        /* Style the buttons inside the tab */
        .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
        }
        
        /* Change background color of buttons on hover */
        .tab button:hover {
        background-color: #ddd;
        }
        
        /* Create an active/current tablink class */
        .tab button.active {
        background-color: #ccc;
        }
        
        /* Style the tab content */
        .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        }

        .gallery{
            display: inline-block;
            margin-top: 20px;
        }
        
        .close-icon{
            border-radius: 50%;
            position: absolute;
            right: 5px;
            top: -10px;
            padding: 5px 8px;
        }
        .form-image-upload{
            background: #e8e8e8 none repeat scroll 0 0;
            padding: 15px;
        }
        .img-responsive{
            max-width: 100%;
        }
    </style>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

@endsection

@section('extrajs')
{{-- <script src="asset/js/jquery.datatables.js"></script> --}}
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(function() {
    var table = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'http://dashboardfa.mo/pendanaan/json',
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'lending_type', name: 'lending_type'},
            {data: 'lending_date', name: 'lending_date'},
            {data: 'lending_nominal', name: 'lending_nominal'},
            {data: 'lending_target', name: 'lending_target'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $('body').on('click', '.editProduct', function () {
      var lending_id = $(this).data('lending_id');
      

      $.get("http://dashboardfa.mo/edit_pendanaan" +'/' + lending_id +'/edit', function (data) {
          $('#modelHeading').html("Edit Lending");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#mlending_id').val(data.lending_id);
          $('#mnominal').val(data.lending_nominal);
          $('#mtarget').val(data.lending_target);
      })
   });

   $('#saveBtn').click(function (e) {
        var lending_id=document.getElementById("mlending_id").value;
        e.preventDefault();
        $(this).html('Sending..');
    
        $.ajax({
          data: $('#productForm').serialize(),
          url: "http://dashboardfa.mo/update_pendanaan"+'/' + lending_id,
          type: "POST",
          dataType: 'json',
          success: function (data) {
     
              $('#productForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              $('#ajaxModel').trigger('click');
                table.draw();
         
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });
    
  });
    </script>
    <script type="text/javascript">
        if (count($errors) > 0){
            $('#msgerror').modal('show');
        }
    </script>
@endsection
