@extends('layouts.layout')

@section('tittle')
Beranda
@endsection

@section('content')
    <div class="container-scroller">
        <div class="container-fluid">
        <div class="full-panel">
            <div class="content-wrapper">
              {{-- <input id="datepicker" type="text" class="form-control datetimepicker" style=""  name="datepicker"> --}}
              <div class="row">
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">PENYALURAN PENDANAAN (MTD)</h4>
                            <canvas id="barChart1" style="height:230px"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">LENDER AKTIF</h4>
                            <canvas id="barChart2" style="height:230px"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">O/S PENDANAAN AKTIF</h4>
                            <canvas id="barChart3" style="height:230px"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">Target VS Realisaasi Pendanaan</h4>
                            <canvas id="barChart4" style="height:230px"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">Target VS Realisaasi O/S Pendanaan</h4>
                            <canvas id="barChart5" style="height:230px"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">Target VS Realisaasi O/S Pendanaan Ritel</h4>
                            <canvas id="barChart6" style="height:230px"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        <h4 class="card-title">Area chart</h4>
                        <canvas id="areaChart" style="height:250px"></canvas>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        <h4 class="card-title">Doughnut chart</h4>
                        <canvas id="doughnutChart" style="height:250px"></canvas>
                        </div>
                    </div>
                    </div>
                </div> --}}
                {{-- <div class="row">
                    <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        <h4 class="card-title">Pie chart</h4>
                        <canvas id="pieChart" style="height:250px"></canvas>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        <h4 class="card-title">Scatter chart</h4>
                        <canvas id="scatterChart" style="height:250px"></canvas>
                        </div>
                    </div>
                    </div>
                </div> --}}
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
            <div class="container-fluid clearfix">
                <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                <i class="mdi mdi-heart text-danger"></i>
                </span>
            </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@endsection

@section('extracss')
    <style>
        /* Style the tab */
        .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
        }
        
        /* Style the buttons inside the tab */
        .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
        }
        
        /* Change background color of buttons on hover */
        .tab button:hover {
        background-color: #ddd;
        }
        
        /* Create an active/current tablink class */
        .tab button.active {
        background-color: #ccc;
        }
        
        /* Style the tab content */
        .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        }

        .gallery{
            display: inline-block;
            margin-top: 20px;
        }
        
        .close-icon{
            border-radius: 50%;
            position: absolute;
            right: 5px;
            top: -10px;
            padding: 5px 8px;
        }
        .form-image-upload{
            background: #e8e8e8 none repeat scroll 0 0;
            padding: 15px;
        }
        .img-responsive{
            max-width: 100%;
        }
    </style>
@endsection

@section('extrajs')
{{-- <script  src="/asset/js/ej2-datepicker.min.js"></script> --}}
{{-- <script src="js/chart.js"></script> --}}
<script>
    
    var array = @json($datas);
    obj = JSON.parse(array);
    // console.log(obj.data04);
    ytd_2years_ago = obj.ytd_2years_ago;
    ytd_1years_ago = obj.ytd_1years_ago;
    ytd_this_year = obj.ytd_this_year;
    target_this_year = obj.target_this_year;

    os_ytd_2yearsago = obj.os_ytd_2yearsago;
    os_ytd_1yearsago = obj.os_ytd_1yearsago;
    os_ytd_thisyear = obj.os_ytd_thisyear;
    os_target_thisyear = obj.os_target_thisyear;

    os_1yearago_individu = obj.os_1yearago_individu;
    os_yearnow_individu = obj.os_yearnow_individu;
    os_targetnow_individu = obj.os_targetnow_individu;

    lender_aktif_2yr = obj.lender_aktif_2yr;
    lender_aktif_1yr = obj.lender_aktif_1yr;
    lender_aktif_now = obj.lender_aktif_now;

    // console.log(ytd_2years_ago);

    $(function() {
  /* ChartJS
   * -------
   * Data and config for chartjs
   */
  'use strict';
  var dataytd = {
    labels: ["YTD 2thn lalu", "YTD 1thn lalu", "YTD thn ini"],
    datasets: [{
      label: '# of Votes',
      data: [ytd_2years_ago, ytd_1years_ago,ytd_this_year],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var datayos = {
    labels: ["YTD 2thn lalu", "YTD 1thn lalu", "YTD thn ini"],
    datasets: [{
      label: '# of Votes',
      data: [os_ytd_2yearsago, os_ytd_1yearsago,os_ytd_thisyear],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var datatargetytd = {
    labels: ["YTD thn lalu", "Target", "YTD thn ini"],
    datasets: [{
      label: '# of Votes',
      data: [ytd_1years_ago, target_this_year,ytd_this_year],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var datatargetos = {
    labels: ["YTD thn lalu", "Target", "YTD thn ini"],
    datasets: [{
      label: '# of Votes',
      data: [os_ytd_1yearsago, os_target_thisyear,os_ytd_thisyear],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var dataosindividu = {
    labels: ["YTD thn lalu", "Target", "YTD thn ini"],
    datasets: [{
      label: '# of Votes',
      data: [os_1yearago_individu, os_targetnow_individu,os_yearnow_individu],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var datalender = {
    labels: ["YTD 2thn lalu", "YTD 1thn lalu", "YTD thn ini"],
    datasets: [{
      label: '# of Votes',
      data: [lender_aktif_2yr, lender_aktif_1yr,lender_aktif_now],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };


  var options = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false
    },
    elements: {
      point: {
        radius: 0
      }
    }

  };

  var doughnutPieData = {
    datasets: [{
    data: [ytd_2years_ago, ytd_1years_ago, ytd_this_year],
      backgroundColor: [
        'rgba(75, 192, 192, 0.5)',
        'rgba(153, 102, 255, 0.5)',
        'rgba(255, 159, 64, 0.5)'
      ],
      borderColor: [
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
      '2020-04',
      '2020-05',
      '2020-06',
    ]
  };
  var doughnutPieOptions = {
    responsive: true,
    animation: {
      animateScale: true,
      animateRotate: true
    }
  };
  
  // Get context with jQuery - using jQuery's .get() method.
  if ($("#barChart1").length) {
    var barChartCanvas = $("#barChart1").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: dataytd,
      options: options
    });
  }

  if ($("#barChart2").length) {
    var barChartCanvas2 = $("#barChart2").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart2 = new Chart(barChartCanvas2, {
      type: 'bar',
      data: datalender,
      options: options
    });
  }

  if ($("#barChart3").length) {
    var barChartCanvas3 = $("#barChart3").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart3 = new Chart(barChartCanvas3, {
      type: 'bar',
      data: datayos,
      options: options
    });
  }
  

  if ($("#barChart4").length) {
    var barChartCanvas4 = $("#barChart4").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart4 = new Chart(barChartCanvas4, {
      type: 'bar',
      data: datatargetytd,
      options: options
    });
  }

  
  if ($("#barChart5").length) {
    var barChartCanvas5 = $("#barChart5").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart5 = new Chart(barChartCanvas5, {
      type: 'bar',
      data: datatargetos,
      options: options
    });
  }

  if ($("#barChart6").length) {
    var barChartCanvas6 = $("#barChart6").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart6 = new Chart(barChartCanvas6, {
      type: 'bar',
      data: dataosindividu,
      options: options
    });
  }

//   if ($("#doughnutChart").length) {
//     var doughnutChartCanvas = $("#doughnutChart").get(0).getContext("2d");
//     var doughnutChart = new Chart(doughnutChartCanvas, {
//       type: 'doughnut',
//       data: doughnutPieData,
//       options: doughnutPieOptions
//     });
//   }

//   if ($("#pieChart").length) {
//     var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
//     var pieChart = new Chart(pieChartCanvas, {
//       type: 'pie',
//       data: doughnutPieData,
//       options: doughnutPieOptions
//     });
//   }

});


</script>
@endsection
