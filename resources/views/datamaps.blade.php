@extends('layouts.layout')

@section('tittle')
Maps
@endsection

@section('content')
    <div class="container-scroller">
        <div class="container-fluid">
          <div class="full-panel">
              <div class="content-wrapper">
                {{-- statistics --}}
                <div class="row-lg-12">
                    <div id="vmap" style="width: 900px; height: 400px;"></div>
                </div>
                
              </div>
              <!-- content-wrapper ends -->
              <!-- partial:../../partials/_footer.html -->
              <footer class="footer">
              <div class="container-fluid clearfix">
                  <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                  <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
                  <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                  <i class="mdi mdi-heart text-danger"></i>
                  </span>
              </div>
              </footer>
              <!-- partial -->
          </div>
        <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@endsection

@section('extracss')
    <link href="/asset/css/jqvmap.css" media="screen" rel="stylesheet" type="text/css"/>
    <style>
        /* Style the tab */
        .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
        }
        
        /* Style the buttons inside the tab */
        .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
        }
        
        /* Change background color of buttons on hover */
        .tab button:hover {
        background-color: #ddd;
        }
        
        /* Create an active/current tablink class */
        .tab button.active {
        background-color: #ccc;
        }
        
        /* Style the tab content */
        .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        }

        .gallery{
            display: inline-block;
            margin-top: 20px;
        }
        
        .close-icon{
            border-radius: 50%;
            position: absolute;
            right: 5px;
            top: -10px;
            padding: 5px 8px;
        }
        .form-image-upload{
            background: #e8e8e8 none repeat scroll 0 0;
            padding: 15px;
        }
        .img-responsive{
            max-width: 100%;
        }
        </style>
@endsection

@section('extrajs')
    <script type="text/javascript" src="/asset/js/jquery.vmap.js"></script>
    <script type="text/javascript" src="/asset/js/jquery.vmap.indonesia.js" charset="utf-8"></script>
    <script type="text/javascript" src="/asset/js/jquery.vmap.world.js" charset="utf-8"></script>
    <script>
      var gdpData = {"Aceh":16.63,"Sumatera Utara":11.58}
      jQuery(document).ready(function () {
        jQuery('#vmap').vectorMap({
          map: 'indonesia_id',
          backgroundColor: '#008E9B',
          borderWidth: 3,
          enableZoom: true,
          showTooltip: true,
          selectedColor: null,
          series: {
                    regions: [{
                        values: gdpData,
                        scale: ['#C8EEFF', '#0071A4'],
                        normalizeFunction: 'polynomial',
                    }]
                },
        //   onLabelShow: function (event, label, code) {
        //       if(sample_data[code] > 0)
        //           label.append(': '+sample_data[code]+' Views'); 
        //   },
          onRegionClick: function(event, code, region){
            event.preventDefault();
          },
          onResize: function (element, width, height) {
            console.log('Map Size: ' +  width + 'x' +  height);
          }
        });
      });
    </script>
@endsection
