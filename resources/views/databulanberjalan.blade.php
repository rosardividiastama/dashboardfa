@extends('layouts.layout')

@section('tittle')
Beranda
@endsection

@section('content')
    <div class="container-scroller">
        <div class="container-fluid">
        <div class="full-panel">
            <div class="content-wrapper"> 
                <div class="row">
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">PENYALURAN PENDANAAN (MTD)</h4>
                            <canvas id="barChart1" style="height:200px"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">PENYALURAN PENDANAAN (MTD) PER SEGMENT</h4>
                            <canvas id="barChart2" style="height:200px"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">O/S PENYALURAN PENDANAAN PER SEGMENT</h4>
                            <canvas id="barChart3" style="height:200px"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">PENEMPATAN DANA PENDANAAN INDIVIDU</h4>
                            <canvas id="barChart4" style="height:200px"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">PERBANDINGAN MTD SEGMEN INDIVIDU</h4>
                            <canvas id="barChart5" style="height:200px"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">TRANSAKSI HARI INI</h4>
                            {{-- <canvas id="barChart6" style="height:230px"></canvas> --}}
                              <div class="row">
                                <div class="col-lg-8"><p class="" style="margin-bottom: 0px;">Pendanaan Captive</p></div>
                                <div class="col-lg-4"><p id="lending_captive" class="" style="margin-bottom: 0px;">nominal</p></div>
                              </div>
                              <div class="row">
                                <div class="col-lg-8"><p class="" style="margin-bottom: 0px;">Pendanaan Non Captive</p></div>
                                <div class="col-lg-4"><p id="lending_non_captive" class="" style="margin-bottom: 0px;">nominal</p></div>
                              </div>
                              <div class="row">
                                <div class="col-lg-8"><p class="" style="margin-bottom: 0px;">Pendanaan Individu</p></div>
                                <div class="col-lg-4"><p id="lending_individu" class="" style="margin-bottom: 0px;">nominal</p></div>
                              </div>
                              <div class="row">
                                <div class="col-lg-8"><p class="" style="margin-bottom: 0px;">Deposit Individu</p></div>
                                <div class="col-lg-4"><p id="deposit_now_individu" class="" style="margin-bottom: 0px;">nominal</p></div>
                              </div>
                              <div class="row">
                                <div class="col-lg-8"><p class="" style="margin-bottom: 0px;">Total Deposit</p></div>
                                <div class="col-lg-4"><p id="deposit_now_sum" class="" style="margin-bottom: 0px;">nominal</p></div>
                              </div>
                              <div class="row">
                                <div class="col-lg-8"><p class="" style="margin-bottom: 0px;">Total Withdraw</p></div>
                                <div class="col-lg-4"><p id="withdraw_now_sum" class="" style="margin-bottom: 0px;">nominal</p></div>
                              </div>
                              <div class="row">
                                <div class="col-lg-8"><p class="" style="margin-bottom: 0px;">Pendana Register</p></div>
                                <div class="col-lg-4"><p id="pendana_now" class="" style="margin-bottom: 0px;">nominal</p></div>
                              </div>
                              <div class="row">
                                <div class="col-lg-8"><p class="" style="margin-bottom: 0px;">Pendana Aktif</p></div>
                                <div class="col-lg-4"><p id="pendana_now_aktif" class="" style="margin-bottom: 0px;">nominal</p></div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        <h4 class="card-title">Area chart</h4>
                        <canvas id="areaChart" style="height:250px"></canvas>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        <h4 class="card-title">Doughnut chart</h4>
                        <canvas id="doughnutChart" style="height:250px"></canvas>
                        </div>
                    </div>
                    </div>
                </div> --}}
                {{-- <div class="row">
                    <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        <h4 class="card-title">Pie chart</h4>
                        <canvas id="pieChart" style="height:250px"></canvas>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        <h4 class="card-title">Scatter chart</h4>
                        <canvas id="scatterChart" style="height:250px"></canvas>
                        </div>
                    </div>
                    </div>
                </div> --}}
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
            <div class="container-fluid clearfix">
                <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                <i class="mdi mdi-heart text-danger"></i>
                </span>
            </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@endsection

@section('extracss')
    <style>
        /* Style the tab */
        .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
        }
        
        /* Style the buttons inside the tab */
        .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
        }
        
        /* Change background color of buttons on hover */
        .tab button:hover {
        background-color: #ddd;
        }
        
        /* Create an active/current tablink class */
        .tab button.active {
        background-color: #ccc;
        }
        
        /* Style the tab content */
        .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        }

        .gallery{
            display: inline-block;
            margin-top: 20px;
        }
        
        .close-icon{
            border-radius: 50%;
            position: absolute;
            right: 5px;
            top: -10px;
            padding: 5px 8px;
        }
        .form-image-upload{
            background: #e8e8e8 none repeat scroll 0 0;
            padding: 15px;
        }
        .img-responsive{
            max-width: 100%;
        }
        </style>
@endsection

@section('extrajs')
{{-- <script src="js/chart.js"></script> --}}
<script>
    
    var lending = @json($lending);
    obj = JSON.parse(lending);
    // console.log(obj.data04);
    // console.log(array);
    ytd_1monthsago = obj.ytd_1monthsago;
    ytd_monthnow = obj.ytd_monthnow;
    target_monthnow = obj.target_monthnow;

    mtd_monthsago_individu = obj.mtd_monthsago_individu;
    mtd_monthnow_individu = obj.mtd_monthnow_individu;
    mtd_monthsago_captive = obj.mtd_monthsago_captive;
    mtd_monthnow_captive = obj.mtd_monthnow_captive;
    mtd_monthsago_institusi = obj.mtd_monthsago_institusi;
    mtd_monthnow_institusi = obj.mtd_monthnow_institusi;

    var outstanding = @json($outstanding);
    objoutstanding = JSON.parse(outstanding);

    // console.log(ytd_1monthsago,target_monthnow);

    os_monthsago_individu = objoutstanding.os_monthsago_individu;
    os_monthnow_individu = objoutstanding.os_monthnow_individu;
    os_monthsago_captive = objoutstanding.os_monthsago_captive;
    os_monthnow_captive = objoutstanding.os_monthnow_captive;
    os_monthsago_institusi = objoutstanding.os_monthsago_institusi;
    os_monthnow_institusi = objoutstanding.os_monthnow_institusi;
    
    var penempatan = @json($penempatan);
    objpenempatan = JSON.parse(penempatan);

    // console.log(objpenempatan);

    pd_monthsago = objpenempatan.pd_monthsago;
    pd_monthnow = objpenempatan.pd_monthnow;
    pd_target_monthnow = objpenempatan.pd_target_monthnow;
    // console.log(pd_monthsago,pd_monthnow,pd_target_monthnow);

    var daily_array = @json($daily_array);
    objdaily_array = JSON.parse(daily_array);

    lending_now_individu = objdaily_array.lending_now_individu;
    lending_now_captive = objdaily_array.lending_now_captive;
    lending_now_institusi = objdaily_array.lending_now_institusi;
    deposit_now_individu = objdaily_array.deposit_now_individu;
    deposit_now_sum = objdaily_array.deposit_now_sum;
    pendana_now = objdaily_array.pendana_now;
    pendana_now_aktif = objdaily_array.pendana_now_aktif;
    withdraw_now_sum = objdaily_array.withdraw_now_sum;
    console.log(lending_now_captive);

    // console.log(lending_now_individu,lending_now_captive,lending_now_institusi,deposit_now_individu,deposit_now_sum);

    $(function() {
  /* ChartJS
   * -------
   * Data and config for chartjs
   */
  'use strict';
  var dataytd = {
    labels: ["Bulan lalu", "Target", "Bulan ini"],
    datasets: [{
      label: 'MTD',
      data: [ytd_1monthsago, target_monthnow,ytd_monthnow],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var data_segment = {
    labels: ["Bln lalu", "Bln ini"],
    datasets: [{
      data: [mtd_monthsago_captive, mtd_monthnow_captive],
      label: "Captive",
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(255, 99, 132, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(255,99,132,1)'
      ],
      borderWidth: 1
    }, {data: [mtd_monthsago_individu, mtd_monthnow_individu],
        label: "Individu",
      backgroundColor: [
        'rgba(54, 162, 235, 0.2)',
        'rgba(54, 162, 235, 0.2)'
      ],
      borderColor: [
        'rgba(54, 162, 235, 1)',
        'rgba(54, 162, 235, 1)'
      ],
      borderWidth: 1
    }, {data: [mtd_monthsago_institusi, mtd_monthnow_institusi],
        label: "Institusi",
      backgroundColor: [
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255, 159, 64, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var os_segment = {
    labels: ["Bln lalu", "Bln ini"],
    datasets: [{
      data: [os_monthsago_captive, os_monthnow_captive],
      label: "Captive",
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(255, 99, 132, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(255,99,132,1)'
      ],
      borderWidth: 1
    }, {data: [os_monthsago_individu, os_monthnow_individu],
        label: "Individu",
      backgroundColor: [
        'rgba(54, 162, 235, 0.2)',
        'rgba(54, 162, 235, 0.2)'
      ],
      borderColor: [
        'rgba(54, 162, 235, 1)',
        'rgba(54, 162, 235, 1)'
      ],
      borderWidth: 1
    }, {data: [os_monthsago_institusi, os_monthnow_institusi],
        label: "Institusi",
      backgroundColor: [
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255, 159, 64, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var datapd = {
    labels: ["Bulan lalu", "Target", "Bulan ini"],
    datasets: [{
      label: 'PENEMPATAN',
      data: [pd_monthsago, pd_target_monthnow,pd_monthnow],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };

  var perbandingan = {
    labels: ["Bln lalu", "Bln ini"],
    datasets: [{
      data: [pd_monthsago, pd_monthnow],
      label: "Penempatan",
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(255, 99, 132, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(255,99,132,1)'
      ],
      borderWidth: 1
    }, {data: [mtd_monthsago_individu, mtd_monthnow_individu],
        label: "Pendanaan",
      backgroundColor: [
        'rgba(54, 162, 235, 0.2)',
        'rgba(54, 162, 235, 0.2)'
      ],
      borderColor: [
        'rgba(54, 162, 235, 1)',
        'rgba(54, 162, 235, 1)'
      ],
      borderWidth: 1
    }]
  };

  var options = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: true
    },
    elements: {
      point: {
        radius: 0
      }
    }

  };
  
  // Get context with jQuery - using jQuery's .get() method.
  if ($("#barChart1").length) {
    var barChartCanvas = $("#barChart1").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: dataytd,
      options: options
    });
  }

  if ($("#barChart2").length) {
    var barChartCanvas2 = $("#barChart2").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart2 = new Chart(barChartCanvas2, {
      type: 'bar',
      data: data_segment,
      options: options
    });
  }

  if ($("#barChart3").length) {
    var barChartCanvas3 = $("#barChart3").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart3 = new Chart(barChartCanvas3, {
      type: 'bar',
      data: os_segment,
      options: options
    });
  }

  if ($("#barChart4").length) {
    var barChartCanvas4 = $("#barChart4").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart4 = new Chart(barChartCanvas4, {
      type: 'bar',
      data: datapd,
      options: options
    });
  }

  if ($("#barChart5").length) {
    var barChartCanvas5 = $("#barChart5").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var barChart5 = new Chart(barChartCanvas5, {
      type: 'bar',
      data: perbandingan,
      options: options
    });
  }
  
  document.getElementById("lending_captive").innerHTML = lending_now_captive;
  document.getElementById("lending_non_captive").innerHTML = lending_now_institusi;
  document.getElementById("lending_individu").innerHTML = lending_now_individu;
  document.getElementById("deposit_now_individu").innerHTML = deposit_now_individu;
  document.getElementById("deposit_now_sum").innerHTML = deposit_now_sum;
  document.getElementById("pendana_now").innerHTML = pendana_now;
  document.getElementById("pendana_now_aktif").innerHTML = pendana_now_aktif;
  document.getElementById("withdraw_now_sum").innerHTML = withdraw_now_sum;

});


</script>
@endsection
