<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\tblOutstanding;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class ContohRepo
 * @package App\Repositories
 * @version December 10, 2019, 1:09 pm UTC
*/

class OutstandingRepo extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tblOutstanding::class;
    }

    public function get_os_ytd_2yearsago(){
        $two_year_ago = (Carbon::now()->subYear(2))->toDateString(); //return 2years ago this month
        $start_two_year_ago = (Carbon::now()->startOfYear()->subYear(2))->toDateString(); //return 2years ago first month
        $data=tblOutstanding::whereBetween('outstanding_date',[$start_two_year_ago, $two_year_ago]) //from - now
            ->sum('outstanding_nominal');
        return $data;
    }

    public function get_os_ytd_1yearsago(){
        $one_year_ago = (Carbon::now()->subYear(1))->toDateString(); //return 2years ago this month
        $start_one_year_ago = (Carbon::now()->startOfYear()->subYear(1))->toDateString(); //return 2years ago first month
        $data=tblOutstanding::whereBetween('outstanding_date',[$start_one_year_ago, $one_year_ago]) //from - now
            ->sum('outstanding_nominal');
        return $data;
    }

    public function get_os_ytd_thisyear(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_this_year = (Carbon::now()->startOfYear())->toDateString(); //returns first month day
        $data=tblOutstanding::whereBetween('outstanding_date',[$start_this_year, $now]) //from - now
            ->sum('outstanding_nominal');
        return $data;
    }

    public function get_os_target_thisyear(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_this_year = (Carbon::now()->startOfYear())->toDateString(); //returns first month day
        $data=tblOutstanding::whereBetween('outstanding_date',[$start_this_year, $now]) //from - now
            ->sum('outstanding_target');
        return $data;
    }

    public function get_os_1yearago_individu(){
        $start_one_year_ago = (Carbon::now()->subYear(1)->startOfYear())->toDateString();
        $one_year_ago = (Carbon::now()->subYear(1))->toDateString();
        $data = tblOutstanding::whereBetween("outstanding_date",[$start_one_year_ago, $one_year_ago])
        ->where('outstanding_type','Individu')
        ->sum('outstanding_nominal');
        // dd($data);
        return $data;
    }

    public function get_os_yearnow_individu(){
        $start_year_now = (Carbon::now()->startOfYear())->toDateString();
        $now = (Carbon::now())->toDateString();
        $data = tblOutstanding::whereBetween("outstanding_date",[$start_year_now, $now])
        ->where('outstanding_type','Individu')
        ->sum('outstanding_nominal');
        return $data;
    }

    public function get_os_targetnow_individu(){
        $start_year_now = (Carbon::now()->startOfYear())->toDateString();
        $now = (Carbon::now())->toDateString();
        $data = tblOutstanding::whereBetween("outstanding_date",[$start_year_now, $now])
        ->where('outstanding_type','Individu')
        ->sum('outstanding_target');
        return $data;
    }

    // === chart penyaluran dana persegment
    public function get_os_monthsago_individu(){
        $start_one_month_ago = (Carbon::now()->startOfMonth(1)->subMonth(1))->toDateString();
        $end_one_month_ago = (Carbon::now()->endOfMonth(1)->subMonth(1))->toDateString();
        $data = tblOutstanding::whereBetween("outstanding_date",[$start_one_month_ago, $end_one_month_ago])
        ->where('outstanding_type','Individu')
        ->sum('outstanding_nominal');
        // dd($data);
        return $data;
    }

    public function get_os_monthnow_individu(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data = tblOutstanding::whereBetween("outstanding_date",[$start_month_now, $now])
        ->where('outstanding_type','Individu')
        ->sum('outstanding_nominal');
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }

    public function get_os_monthsago_captive(){
        $start_one_month_ago = (Carbon::now()->startOfMonth(1)->subMonth(1))->toDateString();
        $end_one_month_ago = (Carbon::now()->endOfMonth(1)->subMonth(1))->toDateString();
        $data = tblOutstanding::whereBetween("outstanding_date",[$start_one_month_ago, $end_one_month_ago])
        ->where('outstanding_type','Captive')
        ->sum('outstanding_nominal');
        return $data;
    }

    public function get_os_monthnow_captive(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data = tblOutstanding::whereBetween("outstanding_date",[$start_month_now, $now])
        ->where('outstanding_type','Captive')
        ->sum('outstanding_nominal');
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }

    public function get_os_monthsago_institusi(){
        $start_one_month_ago = (Carbon::now()->startOfMonth(1)->subMonth(1))->toDateString();
        $end_one_month_ago = (Carbon::now()->endOfMonth(1)->subMonth(1))->toDateString();
        $data = tblOutstanding::whereBetween("outstanding_date",[$start_one_month_ago, $end_one_month_ago])
        ->where('outstanding_type','Institusi')
        ->sum('outstanding_nominal');
        return $data;
    }

    public function get_os_monthnow_institusi(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data = tblOutstanding::whereBetween("outstanding_date",[$start_month_now, $now])
        ->where('outstanding_type','Institusi')
        ->sum('outstanding_nominal');
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }

    public function get_os_untilnow(){
        $data=tblOutstanding::sum('outstanding_nominal');
        return $data;
    }

    public function get_os_untilnow_individu(){
        $data=tblOutstanding::where('outstanding_type','Individu')
        ->sum('outstanding_nominal');
        // dd($data);
        return $data;
    }

    public function get_os_untilnow_institusi(){
        $data=tblOutstanding::where('outstanding_type','Institusi')
        ->sum('outstanding_nominal');
        return $data;
    }

    public function get_os_untilnow_captive(){
        $data=tblOutstanding::where('outstanding_type','Captive')
        ->sum('outstanding_nominal');
        return $data;
    }

    public function getAlljson(){
        // return Datatables::of(tblOutstanding::all())->make(true);
        return Datatables::of(tblOutstanding::all())
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                        $btn = '<a href="" data-toggle="modal" data-target="#ajaxModel"  data-outstanding_id="'.$row->outstanding_id.'" data-original-title="Edit" data-dismiss="modal" class="edit btn btn-primary btn-sm editProduct">Edit</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }
}
