<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\tblLending;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class ContohRepo
 * @package App\Repositories
 * @version December 10, 2019, 1:09 pm UTC
*/

class LendingRepo extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tblLending::class;
    }

    // ========= data tahun berjalan =========
    public function get_sum_ytd_2yearsago(){
        $two_year_ago = (Carbon::now()->subYear(2))->toDateString(); //return 2years ago this month
        $start_two_year_ago = (Carbon::now()->startOfYear()->subYear(2))->toDateString(); //return 2years ago first month
        $data=tblLending::whereBetween('lending_date',[$start_two_year_ago, $two_year_ago]) //from - now
            ->sum('lending_nominal');
        return $data;
    }

    public function get_sum_ytd_1yearsago(){
        $one_year_ago = (Carbon::now()->subYear(1))->toDateString(); //return 2years ago this month
        $start_one_year_ago = (Carbon::now()->startOfYear()->subYear(1))->toDateString(); //return 2years ago first month
        $data=tblLending::whereBetween('lending_date',[$start_one_year_ago, $one_year_ago]) //from - now
            ->sum('lending_nominal');
        return $data;
    }

    public function get_sum_ytd_thisyear(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_this_year = (Carbon::now()->startOfYear())->toDateString(); //returns first year day
        $data=tblLending::whereBetween('lending_date',[$start_this_year, $now]) //from - now
            ->sum('lending_nominal');
        return $data;
    }

    public function get_sum_targetytd_thisyear(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_this_year = (Carbon::now()->startOfYear())->toDateString(); //returns first year day
        $data=tblLending::whereBetween('lending_date',[$start_this_year, $now]) //from - now
            ->sum('lending_target');
        return $data;
    }



    // ========= data bulan berjalan =========

    // === chart penyaluran dana bulan
    public function get_sum_mtd_1monthsago(){
        $start_one_month_ago = (Carbon::now()->startOfMonth(1)->subMonth(1))->toDateString();
        $end_one_month_ago = (Carbon::now()->endOfMonth(1)->subMonth(1))->toDateString();
        $data=tblLending::whereBetween('lending_date',[$start_one_month_ago, $end_one_month_ago])->sum('lending_nominal');
        return $data;
    }

    public function get_sum_mtd_monthnow(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data=tblLending::whereBetween('lending_date',[$start_month_now, $now])->sum('lending_nominal');
        return $data;
    }

    public function get_sum_target_monthnow(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); //return 2years ago first month
        $data=tblLending::whereBetween('lending_date',[$start_month_now, $now])->sum('lending_target');
        // $data = tblLending::where("lending_date","=", $start_month_now)->sum('lending_target');
        // dd($data);
        return $data;
    }

    // === chart penyaluran dana persegment
    public function get_mtd_monthsago_individu(){
        $start_one_month_ago = (Carbon::now()->startOfMonth(1)->subMonth(1))->toDateString();
        $end_one_month_ago = (Carbon::now()->endOfMonth(1)->subMonth(1))->toDateString();
        $data = tblLending::whereBetween("lending_date",[$start_one_month_ago, $end_one_month_ago])
        ->where('lending_type','Individu')
        ->sum('lending_nominal');
        // dd($data);
        return $data;
    }

    public function get_mtd_monthnow_individu(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data=tblLending::whereBetween('lending_date',[$start_month_now, $now])
        ->where('lending_type','Individu')
        ->sum('lending_nominal');
        // dd($data);
        if (empty($data)) {
            return 0;   
        } else {
            return $data;
        }
    }

    public function get_mtd_monthsago_captive(){
        $start_one_month_ago = (Carbon::now()->startOfMonth(1)->subMonth(1))->toDateString();
        $end_one_month_ago = (Carbon::now()->endOfMonth(1)->subMonth(1))->toDateString();
        $data = tblLending::whereBetween("lending_date",[$start_one_month_ago, $end_one_month_ago])
        ->where('lending_type','Captive')
        ->sum('lending_nominal');
        // dd($data);
        return $data;
    }

    public function get_mtd_monthnow_captive(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data=tblLending::whereBetween('lending_date',[$start_month_now, $now])
        ->where('lending_type','Captive')
        ->sum('lending_nominal');
        // dd($data);
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }

    public function get_mtd_monthsago_institusi(){
        $start_one_month_ago = (Carbon::now()->startOfMonth(1)->subMonth(1))->toDateString();
        $end_one_month_ago = (Carbon::now()->endOfMonth(1)->subMonth(1))->toDateString();
        $data = tblLending::whereBetween("lending_date",[$start_one_month_ago, $end_one_month_ago])
        ->where('lending_type','Institusi')
        ->sum('lending_nominal');
        return $data;
    }

    public function get_mtd_monthnow_institusi(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data=tblLending::whereBetween('lending_date',[$start_month_now, $now])
        ->where('lending_type','Institusi')
        ->sum('lending_nominal');
        // dd($data);
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }

    public function get_sum_untilnow(){
        $data=tblLending::sum('lending_nominal');
        // dd($data);
        return $data;
    }

    public function getCountById(){
        $data=tblLending::all()
            ->count();
        return $data;
    }

    public function get_now_individu(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $data=tblLending::where('lending_date', $now)
        ->where('lending_type','Individu')
        ->sum('lending_nominal');
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }

    public function get_now_captive(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $data=tblLending::where('lending_date', $now)
        ->where('lending_type','Captive')
        ->sum('lending_nominal');
        // dd($data);
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }
    public function get_now_institusi(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $data=tblLending::where('lending_date', $now)
        ->where('lending_type','Institusi')
        ->sum('lending_nominal');
        // dd($data);
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }

    public function getAlljson(){
        // return Datatables::of(tblLending::all())->make(true);
        return Datatables::of(tblLending::all())
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="" data-toggle="modal" data-target="#ajaxModel"  data-lending_id="'.$row->lending_id.'" data-original-title="Edit" data-dismiss="modal" class="edit btn btn-primary btn-sm editProduct">Edit</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }

    public function get_sum_2thn(){
        // $datas = tblLending::where("lending_date",">", Carbon::now()->subYears(1))->select('lending_nominal')->get();
        $datas = tblLending::where("lending_date",">", Carbon::now()->subYears(2))->sum('lending_nominal');
        return $datas;
    }

    public function get_sum_1thn(){
        // $datas = tblLending::where("lending_date",">", Carbon::now()->subYears(1))->select('lending_nominal')->get();
        $datas = tblLending::where("lending_date",">", Carbon::now()->subYears(1))->sum('lending_nominal');
        return $datas;
    }

    public function get_sum_0thn(){
        // $datas = tblLending::where("lending_date",">", Carbon::now()->subYears(1))->select('lending_nominal')->get();
        $datas = tblLending::where("lending_date",">", Carbon::now()->subYears(0))->sum('lending_nominal');
        return $datas;
    }

    public function get_20b04(){
        $data=tblLending::where('lending_date','2020-04-01')->sum('lending_nominal');
        return $data;
    }

    public function get_20b05(){
        $data=tblLending::where('lending_date','2020-05-01')->sum('lending_nominal');
        return $data;
    }

    public function get_20b06(){
        $data=tblLending::where('lending_date','2020-06-01')->sum('lending_nominal');
        return $data;
    }

    // public function get_sum_last2month(){
    //     $datas = tblLending::where("lending_date","=", Carbon::now()->startOfMonth()->subMonth(3))->sum('lending_nominal');
    //     dd($datas);
    //     return $datas;
    // }

    
}
