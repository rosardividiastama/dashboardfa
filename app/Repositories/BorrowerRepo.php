<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\tblBorrower;
use Carbon\Carbon;

/**
 * Class ContohRepo
 * @package App\Repositories
 * @version December 10, 2019, 1:09 pm UTC
*/

class BorrowerRepo extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tblBorrower::class;
    }

    public function get_sumpeminjam_untilnow(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_year = "2017-11-01"; //returns first month day
        $data=tblBorrower::whereBetween('borrower_tgl_daftar',[$start_year, $now]) //from - now
            ->count('borrower_id');
        return $data;
    }

    public function get_peminjam_male(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_year = "2017-11-01"; //returns first month day
        $data=tblBorrower::whereBetween('borrower_tgl_daftar',[$start_year, $now]) //from - now
            ->where('borrower_gender',"Male")
            ->count('borrower_id');
        return $data;
    }

    public function get_peminjam_female(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_year = "2017-11-01"; //returns first month day
        $data=tblBorrower::whereBetween('borrower_tgl_daftar',[$start_year, $now]) //from - now
            ->where('borrower_gender',"Female")
            ->count('borrower_id');
        return $data;
    }

    public function get_borrower_24thn(){
        $ranges = [ // the start of each age-range.
            '24-' => 24,
            '25-34' => 25,
            '35-55' => 35,
            '55+' => 55
        ];
        $output = tblBorrower::select('borrower_id','borrower_tgl_lahir')->get()
        ->map(function ($date) use ($ranges) {
            $age = Carbon::parse($date->borrower_tgl_lahir)->age;
            // dd($age);
            foreach($ranges as $key => $breakpoint)
            {
                // dd($breakpoint,$key,$age);
                if ($breakpoint >= $age)
                {
                    // dd($key);
                    $date->range = $key;
                    break;
                }
            }
            // dd($date);
            return $date;
        })
        ->mapToGroups(function ($date, $key) {
            return [$date->range => $date];
        })
        ->map(function ($group) {
            return count($group);
        })->toArray();
        
        
        return $output;
    }

    function getBorrowerByEmail($email){
        // dd($searchStatus,$searchAgent);
        $data = tblBorrower::
        where(function($query) use ($email) {
            $query ->where('borrower_email','LIKE','%'.$email.'%');
          })
        ->select('borrower.borrower_nama','borrower.borrower_id')
        ->first();
        return $data;
    }
}
