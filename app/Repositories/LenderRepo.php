<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\tblLender;
use Carbon\Carbon;

/**
 * Class ContohRepo
 * @package App\Repositories
 * @version December 10, 2019, 1:09 pm UTC
*/

class LenderRepo extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tblLender::class;
    }

    public function get_sumpendana_untilnow(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_year = "2017-11-01"; //returns first month day
        $data=tblLender::whereBetween('lender_tgl_daftar',[$start_year, $now]) //from - now
            ->count('lender_id');
        return $data;
    }

    public function get_pendana_male(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_year = "2017-11-01"; //returns first month day
        $data=tblLender::whereBetween('lender_tgl_daftar',[$start_year, $now]) //from - now
            ->where('lender_gender',"Male")
            ->count('lender_id');
        return $data;
    }

    public function get_pendana_female(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_year = "2017-11-01"; //returns first month day
        $data=tblLender::whereBetween('lender_tgl_daftar',[$start_year, $now]) //from - now
            ->where('lender_gender',"Female")
            ->count('lender_id');
        return $data;
    }

    public function get_pendana_aktif_2yrago(){
        $two_year_ago = (Carbon::now()->subYear(2))->toDateString(); //return 2years ago this month
        $start_two_year_ago = (Carbon::now()->startOfYear()->subYear(2))->toDateString(); //return 2years ago first month
        $data=tblLender::whereBetween('lender_tgl_daftar',[$start_two_year_ago, $two_year_ago]) //from - now
            ->where('lender_status',"aktif")
            ->count('lender_id');
            // dd($data);
        return $data;
    }

    public function get_pendana_aktif_1yrago(){
        $one_year_ago = (Carbon::now()->subYear(1))->toDateString(); //return 2years ago this month
        $start_one_year_ago = (Carbon::now()->startOfYear()->subYear(2))->toDateString(); //return 2years ago first month
        $data=tblLender::whereBetween('lender_tgl_daftar',[$start_one_year_ago, $one_year_ago]) //from - now
            ->where('lender_status',"aktif")
            ->count('lender_id');
        return $data;
    }

    public function get_pendana_aktif_thisyear(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_this_year = (Carbon::now()->startOfYear())->toDateString(); //returns first month day
        $data=tblLender::whereBetween('lender_tgl_daftar',[$start_this_year, $now]) //from - now
            ->where('lender_status',"aktif")
            ->count('lender_id');
        return $data;
    }

    public function get_pendana_nonaktif(){
        $two_year_ago = (Carbon::now()->subYear(2))->toDateString(); //return 2years ago this month
        $start_two_year_ago = (Carbon::now()->startOfYear()->subYear(2))->toDateString(); //return 2years ago first month
        $data=tblLender::whereBetween('lender_tgl_daftar',[$start_two_year_ago, $two_year_ago]) //from - now
            ->where('lender_status',"nonaktif")
            ->count('lender_id');
        return $data;
    }

    public function get_pendana_24thn(){
        $ranges = [ // the start of each age-range.
            '24-' => 24,
            '25-34' => 25,
            '35-55' => 35,
            '55+' => 55
        ];
        $output = tblLender::select('lender_id','lender_tgl_lahir')->get()
        ->map(function ($date) use ($ranges) {
            $age = Carbon::parse($date->lender_tgl_lahir)->age;
            // dd($age);
            foreach($ranges as $key => $breakpoint)
            {
                // dd($breakpoint,$key,$age);
                if ($breakpoint >= $age)
                {
                    // dd($key);
                    $date->range = $key;
                    break;
                }
            }
            // dd($date);
            return $date;
        })
        ->mapToGroups(function ($date, $key) {
            return [$date->range => $date];
        })
        ->map(function ($group) {
            return count($group);
        })->toArray();
        
        
        return $output;
    }

    function getLenderByEmail($email){
        // dd($searchStatus,$searchAgent);
        $data = tblLender::
        where(function($query) use ($email) {
            $query ->where('lender_email','LIKE','%'.$email.'%');
          })
        ->select('lender.lender_nama','lender.lender_id')
        ->first();
        return $data;
    }

    public function get_sumpendana_now(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $data=tblLender::where('lender_tgl_daftar',$now) //from - now
            ->count('lender_id');
        return $data;
    }

    public function get_sumpendana_now_aktif(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $data=tblLender::where('lender_tgl_daftar',$now) //from - now
            ->where('lender_status',"aktif")
            ->count('lender_id');
        return $data;
    }


}
