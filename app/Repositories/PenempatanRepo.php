<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\tblPenempatanDana;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class ContohRepo
 * @package App\Repositories
 * @version December 10, 2019, 1:09 pm UTC
*/

class PenempatanRepo extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tblPenempatanDana::class;
    }

    // === chart penempatan dana bulan
    public function get_sum_pd_monthsago(){
        $start_one_month_ago = (Carbon::now()->startOfMonth(1)->subMonth(1))->toDateString();
        $end_one_month_ago = (Carbon::now()->endOfMonth(1)->subMonth(1))->toDateString();
        $data = tblPenempatanDana::whereBetween("penempatan_dana_date",[$start_one_month_ago, $end_one_month_ago])->sum('penempatan_dana_nominal');
        return $data;
    }

    public function get_sum_pd_monthnow(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data = tblPenempatanDana::whereBetween("penempatan_dana_date",[$start_month_now, $now])->sum('penempatan_dana_nominal');
        return $data;
    }

    public function get_sum_target_monthnow(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data = tblPenempatanDana::whereBetween("penempatan_dana_date",[$start_month_now, $now])->sum('penempatan_dana_target');
        return $data;
    }

    // === chart penempatan dana persegment
    public function get_pd_monthsago_individu(){
        $start_one_month_ago = (Carbon::now()->startOfMonth(1)->subMonth(1))->toDateString();
        $end_one_month_ago = (Carbon::now()->endOfMonth(1)->subMonth(1))->toDateString();
        $data = tblPenempatanDana::whereBetween("penempatan_dana_date",[$start_one_month_ago, $end_one_month_ago])
        ->where('penempatan_dana_type','Individu')
        ->sum('penempatan_dana_nominal');
        // dd($data);
        return $data;
    }

    public function get_pd_monthnow_individu(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data = tblPenempatanDana::whereBetween("penempatan_dana_date",[$start_month_now, $now])
        ->where('penempatan_dana_type','Individu')
        ->sum('penempatan_dana_nominal');
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }

    public function get_target_pd_monthnow_individu(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data = tblPenempatanDana::whereBetween("penempatan_dana_date",[$start_month_now, $now])
        ->where('penempatan_dana_type','Individu')
        ->sum('penempatan_dana_target');
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }

    public function get_pd_monthsago_captive(){
        $start_one_month_ago = (Carbon::now()->startOfMonth(1)->subMonth(1))->toDateString();
        $end_one_month_ago = (Carbon::now()->endOfMonth(1)->subMonth(1))->toDateString();
        $data = tblPenempatanDana::whereBetween("penempatan_dana_date",[$start_one_month_ago, $end_one_month_ago])
        ->where('penempatan_dana_type','Captive')
        ->sum('penempatan_dana_nominal');
        return $data;
    }

    public function get_pd_monthnow_captive(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data = tblPenempatanDana::whereBetween("penempatan_dana_date",[$start_month_now, $now])
        ->where('penempatan_dana_type','Captive')
        ->sum('penempatan_dana_nominal');
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }

    public function get_pd_monthsago_institusi(){
        $start_one_month_ago = (Carbon::now()->startOfMonth(1)->subMonth(1))->toDateString();
        $end_one_month_ago = (Carbon::now()->endOfMonth(1)->subMonth(1))->toDateString();
        $data = tblPenempatanDana::whereBetween("penempatan_dana_date",[$start_one_month_ago, $end_one_month_ago])
        ->where('penempatan_dana_type','Institusi')
        ->sum('penempatan_dana_nominal');
        return $data;
    }

    public function get_pd_monthnow_institusi(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $start_month_now = (Carbon::now()->startOfMonth())->toDateString(); 
        $data = tblPenempatanDana::whereBetween("penempatan_dana_date",[$start_month_now, $now])
        ->where('penempatan_dana_type','Institusi')
        ->sum('penempatan_dana_nominal');
        if (empty($data)) {
            return 0;
        } else {
            return $data;
        }
    }

    public function get_now_individu(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $data=tblPenempatanDana::where('penempatan_dana_date', $now)
        ->where('penempatan_dana_type','Individu')
        ->pluck('penempatan_dana_nominal');
        // dd($data);
        if ($data->isEmpty()) {
            return 0;
        } else {
            return $data;
        }
    }

    public function get_now_sum(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $data=tblPenempatanDana::where('penempatan_dana_date', $now)
        ->sum('penempatan_dana_nominal');
        // dd($data);
        return $data;
    }

    public function getAlljson(){
        // return Datatables::of(tblOutstanding::all())->make(true);
        return Datatables::of(tblPenempatanDana::all())
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                        $btn = '<a href="" data-toggle="modal" data-target="#ajaxModel"  data-penempatan_dana_id="'.$row->penempatan_dana_id.'" data-original-title="Edit" data-dismiss="modal" class="edit btn btn-primary btn-sm editProduct">Edit</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }

}
