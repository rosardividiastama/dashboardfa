<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\tblWithdraw;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class ContohRepo
 * @package App\Repositories
 * @version December 10, 2019, 1:09 pm UTC
*/

class WithdrawRepo extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tblWithdraw::class;
    }

    public function get_now_sum(){
        $now = (Carbon::now())->toDateString(); //returns current day
        $data=tblWithdraw::where('withdraw_date', $now)
        ->sum('withdraw_nominal');
        // dd($data);
        return $data;
    }

    public function getAlljson(){
        return Datatables::of(tblWithdraw::all())
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="" data-toggle="modal" data-target="#ajaxModel"  data-withdraw_id="'.$row->withdraw_id.'" data-original-title="Edit" data-dismiss="modal" class="edit btn btn-primary btn-sm editProduct">Edit</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }

    

    
}
