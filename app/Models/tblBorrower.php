<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tblBorrower extends Model
{
    //
    public $table = 'borrower';
	public $primaryKey = 'borrower_id';
    public $incrementing = true;
    
    protected $guarded = [
    	'created_at','updated_at'  
    ];

    public $fillable = [
        'borrower_id',
        'borrower_nama',
        'borrower_email',
        'borrower_tgl_daftar',
        'borrower_tgl_lahir',
        'borrower_gender',
        'borrower_provinsi',
        'borrower_status'
    ];
}
