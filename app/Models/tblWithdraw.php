<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tblWithdraw extends Model
{
    //
    public $table = 'withdraw';
	public $primaryKey = 'withdraw_id';
    public $incrementing = true;
    
    protected $guarded = [
    	'created_at','updated_at'  
    ];

    public $fillable = [
        'withdraw_id',
        'withdraw_date',
        'withdraw_nominal',
        'withdraw_target',
        'withdraw_type'
    ];
}
