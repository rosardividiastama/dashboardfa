<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tblLending extends Model
{
    //
    public $table = 'lending';
	public $primaryKey = 'lending_id';
    public $incrementing = true;
    
    protected $guarded = [
    	'created_at','updated_at'  
    ];

    public $fillable = [
        'lending_id',
        'lending_date',
        'lending_nominal',
        'lending_target',
        'lending_type'
    ];
}
