<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tblOutstanding extends Model
{
    //
    public $table = 'outstanding';
	public $primaryKey = 'outstanding_id';
    public $incrementing = true;
    
    protected $guarded = [
    	'created_at','updated_at'  
    ];

    public $fillable = [
        'outstanding_id',
        'outstanding_date',
        'outstanding_nominal',
        'outstanding_target',
        'outstanding_type',
    ];
}
