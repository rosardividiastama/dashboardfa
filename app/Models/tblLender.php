<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tblLender extends Model
{
    //
    public $table = 'lender';
	public $primaryKey = 'lender_id';
    public $incrementing = true;
    
    protected $guarded = [
    	'created_at','updated_at'  
    ];

    public $fillable = [
        'lender_id',
        'lender_nama',
        'lender_email',
        'lender_tgl_daftar',
        'lender_tgl_lahir',
        'lender_gender',
        'lender_provinsi',
        'lender_status'
    ];
}
