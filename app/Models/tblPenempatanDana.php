<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tblPenempatanDana extends Model
{
    //
    public $table = 'penempatan_dana';
	public $primaryKey = 'penempatan_dana_id';
    public $incrementing = true;
    
    protected $guarded = [
    	'created_at','updated_at'  
    ];

    public $fillable = [
        'penempatan_dana_id',
        'penempatan_dana_date',
        'penempatan_dana_nominal',
        'penempatan_dana_target',
        'penempatan_dana_type'
    ];
}
