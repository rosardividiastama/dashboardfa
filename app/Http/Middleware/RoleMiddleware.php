<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role1='',$role2='',$role3='',$role4='',$role5='',$role6='')
    {

        $userrole=$request->auth->internal_type;
        //dd($userrole);
        if ( !empty($userrole) && ($userrole==$role1 || $userrole==$role2 || $userrole==$role3 || $userrole==$role4|| $userrole==$role5|| $userrole==$role6)) {
            return $next($request);
        }
        return response()->json([
            'response'=>401,
            'message'=>'Unauthorized',
            'data'=>''
        ],401);
    }
}
