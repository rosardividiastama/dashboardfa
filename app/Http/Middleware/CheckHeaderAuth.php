<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class CheckHeaderAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd($request);
        $headerkey=$request->header('Key-auth');
        $user = DB::table('fa_auth')->where('auth_key',$headerkey)->first();
        if(!empty($user)){
            return $next($request);
        }else{
            return response()->json([
                'response' => [
                    'status'=>'403',
                    'message' => 'Unauthorized!!',
                ]
            ],403);
        }
       
        
    }
}
