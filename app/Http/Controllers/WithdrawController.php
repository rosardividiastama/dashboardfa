<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\WithdrawRepo;
use DB;
use Datatables;
use App\Models\tblWithdraw;

class WithdrawController extends Controller
{
    //
    private $withdrawRepoGlobal;

    public function __construct(WithdrawRepo $withdrawRepoObj)
    {
        $this->withdrawRepoGlobal = $withdrawRepoObj;
    }

    // base function ===================

    public function index(Request $request){
        return view('admin_withdraw');
    }

    public function index_json(){
        $data =$this->withdrawRepoGlobal->getAlljson();
        return $data;
        // dd($data);
    }
    
    //CRUD
    public function create(){
    }

    //method post
    public function store(Request $request){
        $this::validate($request,[
            'date'=> 'required',
            'type'=> 'required',
            'nominal'=> 'required|',
            'target'=> 'required|',
        ]);

        $field =array(
                ['withdraw_date', '=', $request->date],
                ['withdraw_type', 'like', $request->type]
              );
        $columns = ['withdraw_id','withdraw_date'];
        $finddata= $this->withdrawRepoGlobal->findByFields($field, $columns);
        // dd($finddata);

        if (empty($finddata)) {
            # belum ada data jdi insert
            $array=array(
                'withdraw_date'=>$request->date,
                'withdraw_type'=>$request->type,
                'withdraw_nominal'=>$request->nominal,
                'withdraw_target'=>$request->target,
            );
            try {
                //code...
                DB::beginTransaction();
                $this->withdrawRepoGlobal->create($array);
                DB::commit();
    
                return redirect()->route('WithdrawController.index')
                ->with('isSukses','1')
                ->with('message','Input sukses');
    
            } catch (\Exception $th) {
                DB::rollback();
                // throw $th;
                return redirect()->route('WithdrawController.index')
                ->with('isSukses','2')
                ->with('message',$th);
            }
        } else {
            # sudah ada data jdi update
            return redirect()->route('WithdrawController.index')
                ->with('isSukses','2')
                ->with('message','Data periode yang di pilih sudah ada, silahkan update');
        }
    }

    //method get
    public function show($id=''){
    }

    //method get
    public function edit($id){
        $tblWithdraw = tblWithdraw::find($id);
        return response()->json($tblWithdraw);

    }

    //method put
    public function update(Request $request){
        tblWithdraw::updateOrCreate(['withdraw_id' => $request->mwithdraw_id],['withdraw_nominal' => $request->mnominal, 'withdraw_target' => $request->mtarget]);        
        return response()->json(['success'=>'Data saved successfully.']);
    }

    //method delete
    public function destroy($id){
    }

    // function tambahan ===================
    private function DataResponse($data=[],$message='',$httpresponse='201'){
        return response()->json([
            'response'=>$httpresponse,
            'message'=>$message,
            'data'=>$data
        ],$httpresponse);
    }
}
