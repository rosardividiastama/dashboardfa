<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Repositories\LenderRepo;
use App\Models\tblLender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use DB;

class apiPostLenderController extends Controller
{
    //
    private $dataPostLenderRepoGlobal;

    public function __construct(LenderRepo $dataPostLenderRepoObj)
    {
        $this->dataPostLenderRepoGlobal = $dataPostLenderRepoObj;
    }

    public function index(Request $request){
    }
    
    //CRUD
    public function create(){
    }

    //method post
    public function store(Request $request){

        // dd($request);
        $this::validate($request,[
            'lender_nama'=>'required',
            'lender_email'=>'required',
            'lender_tgl_daftar'=>'required',
            'lender_tgl_lahir'=>'required',
            'lender_gender'=>'required',
            'lender_provinsi'=>'required',
            'lender_status'=>'required',

        ]);
        $lastID =tblLender::latest()->first();
        // dd($lastID->lender_id);
        $array=array(
            'lender_id'=>intval($lastID->lender_id)+1,
            'lender_nama'=>$request->lender_nama,
            'lender_email'=>$request->lender_email,
            'lender_tgl_daftar'=>$request->lender_tgl_daftar,
            'lender_tgl_lahir'=>$request->lender_tgl_lahir,
            'lender_gender'=>$request->lender_gender,
            'lender_provinsi'=>$request->lender_provinsi,
            'lender_status'=>$request->lender_status,
        );
        // dd($array);

        try {
            //code...
            DB::beginTransaction();
            $datas = $this->dataPostLenderRepoGlobal->create($array);
            DB::commit();

            $arrayreponse = array('lender_id'=>$datas->lender_id);
            $pesan="Berhasil Tambah Data";
            return $this->DataResponse($arrayreponse,$pesan,201);

        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $arrayreponse = array('lender_id'=>$datas->lender_id);
            $pesan="Gagal Tambah Data";
            return $this->DataResponse($arrayreponse,$pesan,422);
        }
    }

    //method get
    public function show($id =''){
        //
    }

    //method get
    public function edit($id){
        //code
    }

    //method put
    public function update($id, Request $request){   
        // dd($request->all());
    }

    //method delete
    public function destroy($id){
        //
    }

    // function tambahan ===================

    private function DataResponse($data=[],$message='',$httpresponse='201'){
        return response()->json([
            'response'=>$httpresponse,
            'message'=>$message,
            'data'=>$data
        ],$httpresponse);
    }

    function importfile(Request $request){
        // dd('masuk');
        $this::validate($request,[
            'file' => 'required|mimes:csv,txt|required|max:10000'
        ]);

        try {
            $arraycsv=$this->csvToArray($request->file);
       } catch (\Exception $e) {
           dd($e);
       }

       if(empty($arraycsv[0]['nama'])||empty($arraycsv[0]['email'])||empty($arraycsv[0]['tgl_daftar'])||empty($arraycsv[0]['tgl_lahir'])||empty($arraycsv[0]['gender'])||empty($arraycsv[0]['provinsi'])||empty($arraycsv[0]['status'])){
        dd('empty data');
        }
        // dd($arraycsv);
        try{
    		DB::beginTransaction();
    		// mulai perulangan insert$
    		$i=1;
    		foreach ($arraycsv as $data) {
                $i++;
                $validator = Validator::make($data,[
                'nama'=> 'required|max:79|regex:/^[\w-,. ]*$/',
                'email'=> 'required|max:79|unique:lender,lender_email|regex:/^[\w- .,@-_]*$/',
                'tgl_daftar'=> 'required', 
                'tgl_lahir'=> 'required', 
                'gender'=> 'required', 
                'provinsi'=> 'required',
                'status'=> 'required',
                ]); 
                if ($validator->fails()){
                    $arrayreponse = array($validator);
                    $pesan='Data Error Di Line '.$i .' Periksa kembali file CSV anda';
                    return $this->DataResponse($arrayreponse,$pesan,422);
                }

                $findEmail = $this->dataPostLenderRepoGlobal->getLenderByEmail($data['email']);

                        if (!empty($findEmail)) {
                            $array=array(
                                'lender_nama'=>$data['nama'],
                                'lender_email'=>$data['email'],
                                'lender_tgl_daftar'=>$data['tgl_daftar'],
                                'lender_tgl_lahir'=>$data['tgl_lahir'],
                                'lender_gender'=>$data['gender'],
                                'lender_provinsi'=>$data['provinsi'],
                                'lender_status'=>$data['status']
                            );
                            DB::commit();
                            $arrayreponse = array($array);
                            $pesan="Gagal Import Data sudah ada";
                            return $this->DataResponse($arrayreponse,$pesan,422);
                            
                        } else {
                            $lastID =tblLender::latest()->last();
                            // dd($lastID);
                            $data_lender= array(
                                'lender_nama'=>$data['nama'],
                                'lender_email'=>$data['email'],
                                'lender_tgl_daftar'=>$data['tgl_daftar'],
                                'lender_tgl_lahir'=>$data['tgl_lahir'],
                                'lender_gender'=>$data['gender'],
                                'lender_provinsi'=>$data['provinsi'],
                                'lender_status'=>$data['status'],
                                'lender_id'=>intval($lastID->lender_id)+1,
                                );
                                // dd($data_borrower);
                                $this->dataPostLenderRepoGlobal->create($data_lender);
                                DB::commit();
                        }
                    }
                //end perulangan insert
                $arrayreponse = array($data_lender);
                $pesan="Sukses Import Data";
                return $this->DataResponse($arrayreponse,$pesan,201);
               
            }catch (\Exception $e) {
                dd($e);
                DB::rollback();
                $arrayreponse = array($arraycsv);
                $pesan="Error Import Data";
                return $this->DataResponse($arrayreponse,$e,422);
        
            }
    }

    function csvToArray($filename = '', $delimiter = ','){

        // dd($filename);
       
        if (!file_exists($filename) || !is_readable($filename))
            return false;
        
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }
}
