<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Repositories\BorrowerRepo;
use App\Models\tblBorrower;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


use DB;

class apiPostBorrowerController extends Controller
{
    //
    private $dataPostBorrowerRepoGlobal;

    public function __construct(BorrowerRepo $dataPostBorrowerRepoObj)
    {
        $this->dataPostBorrowerRepoGlobal = $dataPostBorrowerRepoObj;
    }

    public function index(Request $request){
    }
    
    //CRUD
    public function create(){
    }

    //method post
    public function store(Request $request){

        // dd($request);
        $this::validate($request,[
            'borrower_nama'=>'required',
            'borrower_email'=>'required',
            'borrower_tgl_daftar'=>'required',
            'borrower_tgl_lahir'=>'required',
            'borrower_gender'=>'required',
            'borrower_provinsi'=>'required',
            'borrower_status'=>'required'
        ]);

        $lastID =tblBorrower::latest()->first();
        // dd($lastID);
        $array=array(
            'borrower_id'=>intval($lastID->borrower_id)+1,
            'borrower_nama'=>$request->borrower_nama,
            'borrower_email'=>$request->borrower_email,
            'borrower_tgl_daftar'=>$request->borrower_tgl_daftar,
            'borrower_tgl_lahir'=>$request->borrower_tgl_lahir,
            'borrower_gender'=>$request->borrower_gender,
            'borrower_provinsi'=>$request->borrower_provinsi,
            'borrower_status'=>$request->borrower_status,
        );
        // dd($array);

        try {
            //code...
            DB::beginTransaction();
            $datas = $this->dataPostBorrowerRepoGlobal->create($array);
            DB::commit();

            $arrayreponse = array('borrower_id'=>$datas->borrower_id);
            $pesan="Berhasil Tambah Data";
            return $this->DataResponse($arrayreponse,$pesan,201);

        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $arrayreponse = array('borrower_id'=>$datas->borrower_id);
            $pesan="Gagal Tambah Data";
            return $this->DataResponse($arrayreponse,$pesan,422);
        }
    }

    //method get
    public function show($id =''){
        //
    }

    //method get
    public function edit($id){
        //code
    }

    //method put
    public function update($id, Request $request){   
        // dd($request->all());
    }

    //method delete
    public function destroy($id){
        //
    }

    // function tambahan ===================

    private function DataResponse($data=[],$message='',$httpresponse='201'){
        return response()->json([
            'response'=>$httpresponse,
            'message'=>$message,
            'data'=>$data
        ],$httpresponse);
    }

    function importfile(Request $request){
        // dd('masuk');
        $this::validate($request,[
            'file' => 'required|mimes:csv,txt|required|max:10000'
        ]);

        try {
            $arraycsv=$this->csvToArray($request->file);
       } catch (\Exception $e) {
           dd($e);
       }

    //    dd($arraycsv);

       if(empty($arraycsv[0]['nama'])||empty($arraycsv[0]['email'])||empty($arraycsv[0]['tgl_daftar'])||empty($arraycsv[0]['tgl_lahir'])||empty($arraycsv[0]['gender'])||empty($arraycsv[0]['provinsi'])||empty($arraycsv[0]['status'])){
        dd('empty data');
        }
        // dd($arraycsv);
        try{
    		DB::beginTransaction();
    		// mulai perulangan insert$
    		$i=1;
    		foreach ($arraycsv as $data) {
                $i++;
                $validator = Validator::make($data,[
                'nama'=> 'required|max:79|regex:/^[\w-,. ]*$/',
                'email'=> 'required|max:79|unique:borrower,borrower_email|regex:/^[\w- .,@-_]*$/',
                'tgl_daftar'=> 'required', 
                'tgl_lahir'=> 'required', 
                'gender'=> 'required', 
                'provinsi'=> 'required',
                'status'=> 'required',
                ]); 
                if ($validator->fails()){
                    $arrayreponse = array($validator);
                    $pesan='Data Error Di Line '.$i .' Periksa kembali file CSV anda';
                    return $this->DataResponse($arrayreponse,$pesan,422);

                    // return redirect()->back()->withErrors($validator)->withInput()
                    //     ->with('message','Data Error Di Line '.$i .' Periksa kembali file CSV anda' );;
                }

                $findEmail = $this->dataPostBorrowerRepoGlobal->getBorrowerByEmail($data['email']);

                        if (!empty($findEmail)) {
                            // dd('fail');
                            $array=array(
                                'borrower_nama'=>$data['nama'],
                                'borrower_email'=>$data['email'],
                                'borrower_tgl_daftar'=>$data['tgl_daftar'],
                                'borrower_tgl_lahir'=>$data['tgl_lahir'],
                                'borrower_gender'=>$data['gender'],
                                'borrower_provinsi'=>$data['provinsi'],
                                'borrower_status'=>$data['status']
                            );

                            DB::commit();
                            $arrayreponse = array($array);
                            $pesan="Gagal Import Data sudah ada";
                            return $this->DataResponse($arrayreponse,$pesan,422);
                            
                        } else {
                            $lastID =tblBorrower::latest()->first();
                            // dd($lastID);
                            $data_borrower= array(
                                'borrower_nama'=>$data['nama'],
                                'borrower_email'=>$data['email'],
                                'borrower_tgl_daftar'=>$data['tgl_daftar'],
                                'borrower_tgl_lahir'=>$data['tgl_lahir'],
                                'borrower_gender'=>$data['gender'],
                                'borrower_provinsi'=>$data['provinsi'],
                                'borrower_status'=>$data['status'],
                                'borrower_id'=>intval($lastID->borrower_id)+1,
                                );
                                // dd($data_borrower);
                                $this->dataPostBorrowerRepoGlobal->create($data_borrower);
                                DB::commit();
                        }
                    }
                //end perulangan insert
                $arrayreponse = array($data_borrower);
                $pesan="Sukses Import Data";
                return $this->DataResponse($arrayreponse,$pesan,201);
               
            }catch (\Exception $e) {
                dd($e);
                DB::rollback();
                $arrayreponse = array($arraycsv);
                $pesan="Error Import Data";
                return $this->DataResponse($arrayreponse,$e,422);
        
            }
    }

    function csvToArray($filename = '', $delimiter = ','){

        // dd($filename);
       
        if (!file_exists($filename) || !is_readable($filename))
            return false;
        
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }
}
