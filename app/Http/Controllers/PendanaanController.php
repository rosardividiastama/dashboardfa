<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\LendingRepo;

use DB;
use Datatables;
use App\Models\tblLending;


class PendanaanController extends Controller
{
    //
    private $lendingRepoGlobal;

    public function __construct(LendingRepo $lendingRepoObj)
    {
        $this->lendingRepoGlobal = $lendingRepoObj;
    }

    // base function ===================

    public function index(Request $request){    
        return view('admin_pendanaan');
    }

    public function index_json(){
        $data =$this->lendingRepoGlobal->getAlljson();
        return $data;
    }
    
    //CRUD
    public function create(){
    }

    //method post
    public function store(Request $request){
        // dd($request->all());

        $this::validate($request,[
            'date'=> 'required',
            'type'=> 'required',
            'nominal'=> 'required|',
            'target'=> 'required|',
        ]);

        $field =array(
                ['lending_date', '=', $request->date],
                ['lending_type', 'like', $request->type]
              );
        $columns = ['lending_id','lending_date'];
        $finddata= $this->lendingRepoGlobal->findByFields($field, $columns);
        // dd($finddata);

        if (empty($finddata)) {
            # belum ada data jdi insert
            $array=array(
                'lending_date'=>$request->date,
                'lending_type'=>$request->type,
                'lending_nominal'=>$request->nominal,
                'lending_target'=>$request->target,
            );
            try {
                //code...
                DB::beginTransaction();
                $this->lendingRepoGlobal->create($array);
                DB::commit();
    
                return redirect()->route('PendanaanController.index')
                ->with('isSukses','1')
                ->with('message','Input sukses');
    
            } catch (\Exception $th) {
                DB::rollback();
                // throw $th;
                return redirect()->route('PendanaanController.index')
                ->with('isSukses','2')
                ->with('message',$th);
            }
        } else {
            # sudah ada data jdi update
            return redirect()->route('PendanaanController.index')
                ->with('isSukses','2')
                ->with('message','Data periode yang di pilih sudah ada, silahkan update');
        }
    }

    //method get
    public function show($id=''){
    }

    //method get
    public function edit($id){
        $product = tblLending::find($id);
        return response()->json($product);
    }

    //method put
    public function update(Request $request){
        // dd($request);
        tblLending::updateOrCreate(['lending_id' => $request->mlending_id],['lending_nominal' => $request->mnominal, 'lending_target' => $request->mtarget]);        
        return response()->json(['success'=>'Data saved successfully.']);
    }

    //method delete
    public function destroy($id){
    }

    // function tambahan ===================
    private function DataResponse($data=[],$message='',$httpresponse='201'){
        return response()->json([
            'response'=>$httpresponse,
            'message'=>$message,
            'data'=>$data
        ],$httpresponse);
    }
}
