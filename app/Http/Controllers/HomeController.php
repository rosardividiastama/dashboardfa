<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\LendingRepo;
use App\Repositories\OutstandingRepo;
use App\Repositories\LenderRepo;

// use Symfony\Component\HttpFoundation\Session\Session;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     return view('home');
    // }

    private $lendingRepoGlobal;
    private $outstandingRepoGlobal;
    private $lenderRepoGlobal;

    public function __construct(LendingRepo $lendingRepoObj, OutstandingRepo $outstandingRepoObj,LenderRepo $lenderRepoObj )
    {
        $this->middleware('auth');
        $this->lendingRepoGlobal = $lendingRepoObj;
        $this->outstandingRepoGlobal = $outstandingRepoObj;
        $this->lenderRepoGlobal = $lenderRepoObj;
    }

    // base function ===================

    public function index(Request $request){

        $value = Session::all();
        $url = Session::get('_flash.old');
        // dd($value);

        if (empty($url)) {
            
            // dd(date("yy-m-01"));
        } else {
            // dd('ada');
        }
        
        //data penyaluran dana
        $ytd_2years_ago = $this->lendingRepoGlobal->get_sum_ytd_2yearsago();
        $ytd_1years_ago = $this->lendingRepoGlobal->get_sum_ytd_1yearsago();
        $ytd_this_year = $this->lendingRepoGlobal->get_sum_ytd_thisyear();
        $target_this_year = $this->lendingRepoGlobal->get_sum_targetytd_thisyear();

        //data OS pendanaan aktif
        $os_ytd_2yearsago = $this->outstandingRepoGlobal->get_os_ytd_2yearsago();
        $os_ytd_1yearsago = $this->outstandingRepoGlobal->get_os_ytd_1yearsago();
        $os_ytd_thisyear = $this->outstandingRepoGlobal->get_os_ytd_thisyear();
        $os_target_thisyear = $this->outstandingRepoGlobal->get_os_target_thisyear();
        
        $os_1yearago_individu = $this->outstandingRepoGlobal->get_os_1yearago_individu();
        $os_yearnow_individu = $this->outstandingRepoGlobal->get_os_yearnow_individu();
        $os_targetnow_individu = $this->outstandingRepoGlobal->get_os_targetnow_individu();
        // dd($os_1yearago_individu);

        $lender_aktif_2yr = $this->lenderRepoGlobal->get_pendana_aktif_2yrago();
        $lender_aktif_1yr = $this->lenderRepoGlobal->get_pendana_aktif_1yrago();
        $lender_aktif_now = $this->lenderRepoGlobal->get_pendana_aktif_thisyear();
        $lender_nonaktif = $this->lenderRepoGlobal->get_pendana_nonaktif();


        // dd($os_1yearago_individu,$os_yearnow_individu,$os_targetnow_individu, "mantap");
        $array=array(
            'ytd_2years_ago'=>$this->nice_number($ytd_2years_ago),
            'ytd_1years_ago'=>$this->nice_number($ytd_1years_ago),
            'ytd_this_year'=>$this->nice_number($ytd_this_year),
            'os_ytd_2yearsago' =>$this->nice_number($os_ytd_2yearsago),
            'os_ytd_1yearsago' =>$this->nice_number($os_ytd_1yearsago),
            
            'os_ytd_thisyear' =>$this->nice_number($os_ytd_thisyear),
            'target_this_year' =>$this->nice_number($target_this_year),
            'os_target_thisyear' =>$this->nice_number($os_target_thisyear),

            'os_1yearago_individu' =>$this->nice_number($os_1yearago_individu),
            'os_yearnow_individu' =>$this->nice_number($os_yearnow_individu),
            'os_targetnow_individu' =>$this->nice_number($os_targetnow_individu),

            'lender_aktif_2yr' =>$lender_aktif_2yr,
            'lender_aktif_1yr' =>$lender_aktif_1yr,
            'lender_aktif_now' =>$lender_aktif_now,
            'lender_nonaktif' =>$lender_nonaktif
        );

        // dd($array);
        if (empty($ytd_2years_ago)||empty($ytd_1years_ago)||empty($ytd_this_year)||empty($os_ytd_2yearsago)||empty($os_ytd_1yearsago)||empty($os_ytd_thisyear)) {
            $pesan="Data Account ga ketemu"; return $this->DataResponse($array,$pesan,404);
        }else{
            return view('home',[
                'datas'=>json_encode($array)
            ]);
            // return json_encode($array);
        }
    }
    
    //CRUD
    public function create(){
    }

    //method post
    public function store(Request $request){
        // dd($request);
        $response = array(
            'status' => 'success',
            'msg' => $request->message,
        );
        return response()->json($response); 

    }

    //method get
    public function show($id=''){
    }

    //method get
    public function edit($id){
    }

    //method put
    public function update($id, Request $request){
    }

    //method delete
    public function destroy($id){
    }

    // function tambahan ===================
    private function DataResponse($data=[],$message='',$httpresponse='201'){
        return response()->json([
            'response'=>$httpresponse,
            'message'=>$message,
            'data'=>$data
        ],$httpresponse);
    }

    function nice_number($n) {
        // first strip any formatting;
        if ($n==null) {
            return 0;
        } else {
            # code...
            $n = (0+str_replace(",", "", $n));
            if ($n > 0) return round(($n/1000000000), 2);
            elseif ($n == 0) return 0 ;
        }
        return number_format($n);
    }
}
