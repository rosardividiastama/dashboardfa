<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\LendingRepo;
use App\Repositories\OutstandingRepo;
use App\Repositories\PenempatanRepo;
use App\Repositories\LenderRepo;
use App\Repositories\BorrowerRepo;

class AkumulasiController extends Controller
{
    //
    private $lendingRepoGlobal;
    private $outstandingRepoGlobal;
    private $penempatanRepoGlobal;
    private $lenderRepoGlobal;
    private $borrowerRepoGlobal;

    public function __construct(LendingRepo $lendingRepoObj, OutstandingRepo $outstandingRepoObj,PenempatanRepo $penempatanRepoObj,LenderRepo $lenderRepoObj,BorrowerRepo $borrowerRepoObj)
    {
        $this->middleware('auth');
        $this->lendingRepoGlobal = $lendingRepoObj;
        $this->outstandingRepoGlobal = $outstandingRepoObj;
        $this->penempatanRepoGlobal = $penempatanRepoObj;
        $this->lenderRepoGlobal = $lenderRepoObj;
        $this->borrowerRepoGlobal = $borrowerRepoObj;
    }

    // base function ===================

    public function index(Request $request){
        //data penyaluran dana
        $sum_untilnow = $this->lendingRepoGlobal->get_sum_untilnow();
        $pendana_untilnow = $this->lenderRepoGlobal->get_sumpendana_untilnow();
        $peminjam_untilnow = $this->borrowerRepoGlobal->get_sumpeminjam_untilnow();
        $os_untilnow = $this->outstandingRepoGlobal->get_os_untilnow();

        $totalpendana = $this->lenderRepoGlobal->get_pendana_male() + $this->lenderRepoGlobal->get_pendana_female();
        $perpendana_male=$this->lenderRepoGlobal->get_pendana_male()/$totalpendana;
        $perpendana_female=$this->lenderRepoGlobal->get_pendana_female()/$totalpendana;

        // dd($perpendana_male,$perpendana_female);

        $totalpeminjam= $this->borrowerRepoGlobal->get_peminjam_male() + $this->borrowerRepoGlobal->get_peminjam_female();
        $peminjam_male = $this->borrowerRepoGlobal->get_peminjam_male()/$totalpeminjam;
        $peminjam_female = $this->borrowerRepoGlobal->get_peminjam_female()/$totalpeminjam;

        // dd($totalpeminjam);


        $agependana = $this->lenderRepoGlobal->get_pendana_24thn();
        $ageborrower = $this->borrowerRepoGlobal->get_borrower_24thn();

        // $os_untilnow_individu = $this->outstandingRepoGlobal->get_os_untilnow_individu();
        // $os_untilnow_captive = $this->outstandingRepoGlobal->get_os_untilnow_captive();
        // $os_untilnow_institusi = $this->outstandingRepoGlobal->get_os_untilnow_institusi();

        // dd($os_untilnow_individu,$os_untilnow_captive,$os_untilnow_institusi);
        
        $total = $this->outstandingRepoGlobal->get_os_untilnow_individu() + $this->outstandingRepoGlobal->get_os_untilnow_captive() + $this->outstandingRepoGlobal->get_os_untilnow_institusi();

        // dd($total,'total');

        $per_os_untilnow_individu=($this->outstandingRepoGlobal->get_os_untilnow_individu()/$total);
        $per_os_untilnow_captive=($this->outstandingRepoGlobal->get_os_untilnow_captive()/$total);
        $per_os_untilnow_institusi=($this->outstandingRepoGlobal->get_os_untilnow_institusi()/$total);


        // dd($per_os_untilnow_individu,$per_os_untilnow_captive,$per_os_untilnow_institusi,$total);

        $t24an =$agependana["24-"];
        $t25an =$agependana["25-34"];
        $t35an =$agependana["35-55"];
        $t55an =$agependana["55+"];

        $b24an =$ageborrower["24-"];
        $b25an =$ageborrower["25-34"];
        $b35an =$ageborrower["35-55"];
        $b55an =$ageborrower["55+"];
        // foreach ($agependana as $key => $value) {
        //     echo "$key = $value\n";
        // }
        // dd($agependana,$t24an,$t25an,$t35an,$t55an);

        $lendingarray =array(
            'sum_untilnow'=>$sum_untilnow,
            'pendana_untilnow'=>$pendana_untilnow,
            'peminjam_untilnow'=>$peminjam_untilnow,
            'os_untilnow'=>$os_untilnow,
            'pendana_male'=>$perpendana_male,
            'pendana_female'=>$perpendana_female,
            'peminjam_male'=>$peminjam_male,
            'peminjam_female'=>$peminjam_female,
            'os_untilnow_individu'=>$per_os_untilnow_individu,
            'os_untilnow_captive'=>$per_os_untilnow_captive,
            'os_untilnow_institusi'=>$per_os_untilnow_institusi
        );

        $agespendana = array(
            't24an'=>$t24an,
            't25an'=>$t25an,
            't35an'=>$t35an,
            't55an'=>$t55an,
            'b25an'=>$b25an,
            'b35an'=>$b35an,
            'b55an'=>$b55an,
            'b24an'=>$b24an
        );

        // dd($array);
        if (empty($sum_untilnow)) {
            $pesan="Data Account ga ketemu"; return $this->DataResponse($lendingarray,$pesan,404);
        }else{
            return view('dataakumulasi',[
                'lending'=>json_encode($lendingarray),
                'agespendana'=>json_encode($agespendana)
            ]);
            // return json_encode($array);
        }
    }
    
    //CRUD
    public function create(){
    }

    //method post
    public function store(Request $request){
    }

    //method get
    public function show($id=''){
    }

    //method get
    public function edit($id){
    }

    //method put
    public function update($id, Request $request){
    }

    //method delete
    public function destroy($id){
    }

    // function tambahan ===================
    private function DataResponse($data=[],$message='',$httpresponse='201'){
        return response()->json([
            'response'=>$httpresponse,
            'message'=>$message,
            'data'=>$data
        ],$httpresponse);
    }
}
