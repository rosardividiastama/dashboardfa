<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\LenderRepo;
use App\Repositories\BorrowerRepo;

class MapsController extends Controller
{
    //
    private $lenderRepoGlobal;
    private $borrowerRepoGlobal;


    public function __construct(LenderRepo $lenderRepoObj,BorrowerRepo $borrowerRepoObj)
    {
        $this->lenderRepoGlobal = $lenderRepoObj;
        $this->borrowerRepoGlobal = $borrowerRepoObj;
    }

    // base function ===================

    public function index(Request $request){
        return view('datamaps');
    }

    public function index_json(){
    }
    
    //CRUD
    public function create(){
    }

    //method post
    public function store(Request $request){
    }

    //method get
    public function show($id=''){
    }

    //method get
    public function edit($id){
    }

    //method put
    public function update(Request $request){
    }

    //method delete
    public function destroy($id){
    }

    // function tambahan ===================
    private function DataResponse($data=[],$message='',$httpresponse='201'){
        return response()->json([
            'response'=>$httpresponse,
            'message'=>$message,
            'data'=>$data
        ],$httpresponse);
    }
}
