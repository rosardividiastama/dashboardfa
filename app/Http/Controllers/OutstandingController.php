<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\OutstandingRepo;
use DB;
use Datatables;
use App\Models\tblOutstanding;

class OutstandingController extends Controller
{
    //
    private $outstandingRepoGlobal;

    public function __construct(OutstandingRepo $outstandingRepoObj)
    {
        $this->outstandingRepoGlobal = $outstandingRepoObj;
    }

    // base function ===================

    public function index(Request $request){
        return view('admin_outstanding');
    }

    public function index_json(){
        $data =$this->outstandingRepoGlobal->getAlljson();
        return $data;
    }
    
    //CRUD
    public function create(){
    }

    //method post
    public function store(Request $request){
        $this::validate($request,[
            'date'=> 'required',
            'type'=> 'required',
            'nominal'=> 'required|',
            'target'=> 'required|',
        ]);

        $field =array(
                ['outstanding_date', '=', $request->date],
                ['outstanding_type', 'like', $request->type]
                
              );
        $columns = ['outstanding_id','outstanding_date'];
        $finddata= $this->outstandingRepoGlobal->findByFields($field, $columns);
        // dd($finddata);

        if (empty($finddata)) {
            # belum ada data jdi insert
            $array=array(
                'outstanding_date'=>$request->date,
                'outstanding_type'=>$request->type,
                'outstanding_nominal'=>$request->nominal,
                'outstanding_target'=>$request->target,
            );
            try {
                //code...
                DB::beginTransaction();
                $this->outstandingRepoGlobal->create($array);
                DB::commit();
    
                return redirect()->route('OutstandingController.index')
                ->with('isSukses','1')
                ->with('message','Input sukses');
    
            } catch (\Exception $th) {
                DB::rollback();
                // throw $th;
                return redirect()->route('OutstandingController.index')
                ->with('isSukses','2')
                ->with('message',$th);
            }
        } else {
            # sudah ada data jdi update
            return redirect()->route('OutstandingController.index')
                ->with('isSukses','2')
                ->with('message','Data periode yang di pilih sudah ada, silahkan update');
        }
    }

    //method get
    public function show($id=''){
    }

    //method get
    public function edit($id){
        $tblOutstanding = tblOutstanding::find($id);
        return response()->json($tblOutstanding);
    }

    //method put
    public function update(Request $request){
        tblOutstanding::updateOrCreate(['outstanding_id' => $request->moutstanding_id],['outstanding_nominal' => $request->mnominal, 'outstanding_target' => $request->mtarget]);        
        return response()->json(['success'=>'Data saved successfully.']);
    }

    //method delete
    public function destroy($id){
    }

    // function tambahan ===================
    private function DataResponse($data=[],$message='',$httpresponse='201'){
        return response()->json([
            'response'=>$httpresponse,
            'message'=>$message,
            'data'=>$data
        ],$httpresponse);
    }
}
