<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PenempatanRepo;
use DB;
use Datatables;
use App\Models\tblPenempatanDana;


class PenempatanDanaController extends Controller
{
    //
    private $penempatanRepoGlobal;

    public function __construct(PenempatanRepo $penempatanRepoObj)
    {
        $this->penempatanRepoGlobal = $penempatanRepoObj;
    }

    // base function ===================

    public function index(Request $request){
        return view('admin_penempatan_dana');
    }

    public function index_json(){
        $data =$this->penempatanRepoGlobal->getAlljson();
        return $data;
        // dd($data);
    }
    
    //CRUD
    public function create(){
    }

    //method post
    public function store(Request $request){
        $this::validate($request,[
            'date'=> 'required',
            'type'=> 'required',
            'nominal'=> 'required|',
            'target'=> 'required|',
        ]);

        $field =array(
                ['penempatan_dana_date', '=', $request->date],
                ['penempatan_dana_type', 'like', $request->type]
              );
        $columns = ['penempatan_dana_id','penempatan_dana_date'];
        $finddata= $this->penempatanRepoGlobal->findByFields($field, $columns);
        // dd($finddata);

        if (empty($finddata)) {
            # belum ada data jdi insert
            $array=array(
                'penempatan_dana_date'=>$request->date,
                'penempatan_dana_type'=>$request->type,
                'penempatan_dana_nominal'=>$request->nominal,
                'penempatan_dana_target'=>$request->target,
            );
            try {
                //code...
                DB::beginTransaction();
                $this->penempatanRepoGlobal->create($array);
                DB::commit();
    
                return redirect()->route('PenempatanDanaController.index')
                ->with('isSukses','1')
                ->with('message','Input sukses');
    
            } catch (\Exception $th) {
                DB::rollback();
                // throw $th;
                return redirect()->route('PenempatanDanaController.index')
                ->with('isSukses','2')
                ->with('message',$th);
            }
        } else {
            # sudah ada data jdi update
            return redirect()->route('PenempatanDanaController.index')
                ->with('isSukses','2')
                ->with('message','Data periode yang di pilih sudah ada, silahkan update');
        }
    }

    //method get
    public function show($id=''){
    }

    //method get
    public function edit($id){
        $tblPenempatanDana = tblPenempatanDana::find($id);
        return response()->json($tblPenempatanDana);

    }

    //method put
    public function update(Request $request){
        tblPenempatanDana::updateOrCreate(['penempatan_dana_id' => $request->mpenempatan_id],['penempatan_dana_nominal' => $request->mnominal, 'penempatan_dana_target' => $request->mtarget]);        
        return response()->json(['success'=>'Data saved successfully.']);
    }

    //method delete
    public function destroy($id){
    }

    // function tambahan ===================
    private function DataResponse($data=[],$message='',$httpresponse='201'){
        return response()->json([
            'response'=>$httpresponse,
            'message'=>$message,
            'data'=>$data
        ],$httpresponse);
    }
}
