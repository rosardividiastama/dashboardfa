<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\LendingRepo;
use App\Repositories\OutstandingRepo;
use App\Repositories\LenderRepo;
use App\Repositories\BorrowerRepo;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    //
    private $lendingRepoGlobal;
    private $outstandingRepoGlobal;
    private $lenderRepoGlobal;
    private $borrowerRepoGlobal;

    public function __construct(LendingRepo $lendingRepoObj, OutstandingRepo $outstandingRepoObj,LenderRepo $lenderRepoObj, BorrowerRepo $borrowerRepoObj )
    {
        $this->middleware('auth');
        $this->lendingRepoGlobal = $lendingRepoObj;
        $this->outstandingRepoGlobal = $outstandingRepoObj;
        $this->lenderRepoGlobal = $lenderRepoObj;
        $this->borrowerRepoGlobal = $borrowerRepoObj;
    }

    // base function ===================

    public function index(Request $request){
        return view('adminarea');
    }
    
    //CRUD
    public function create(){
    }

    //method post
    public function store(Request $request){
    }

    //method get
    public function show($id=''){
    }

    //method get
    public function edit($id){
    }

    //method put
    public function update($id, Request $request){
    }

    //method delete
    public function destroy($id){
    }

    // function tambahan ===================
    private function DataResponse($data=[],$message='',$httpresponse='201'){
        return response()->json([
            'response'=>$httpresponse,
            'message'=>$message,
            'data'=>$data
        ],$httpresponse);
    }

    
}
