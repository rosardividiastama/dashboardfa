<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\LendingRepo;
use App\Repositories\OutstandingRepo;
use App\Repositories\PenempatanRepo;
use App\Repositories\LenderRepo;
use App\Repositories\WithdrawRepo;

class BulanController extends Controller
{
    private $lendingRepoGlobal;
    private $outstandingRepoGlobal;
    private $penempatanRepoGlobal;
    private $lenderRepoGlobal;
    private $withdrawRepoGlobal;

    public function __construct(LendingRepo $lendingRepoObj, OutstandingRepo $outstandingRepoObj,PenempatanRepo $penempatanRepoObj,LenderRepo $lenderRepoObj,WithdrawRepo $withdrawRepoObj)
    {
        $this->middleware('auth');
        $this->lendingRepoGlobal = $lendingRepoObj;
        $this->outstandingRepoGlobal = $outstandingRepoObj;
        $this->penempatanRepoGlobal = $penempatanRepoObj;
        $this->lenderRepoGlobal = $lenderRepoObj;
        $this->withdrawRepoGlobal = $withdrawRepoObj;
    }

    // base function ===================

    public function index(Request $request){
        //data penyaluran dana
        $ytd_1monthsago = $this->lendingRepoGlobal->get_sum_mtd_1monthsago();
        $ytd_monthnow = $this->lendingRepoGlobal->get_sum_mtd_monthnow();
        $target_monthnow = $this->lendingRepoGlobal->get_sum_target_monthnow();

        //data penyaluran dana persegment
        $mtd_monthsago_individu = $this->lendingRepoGlobal->get_mtd_monthsago_individu();
        $mtd_monthnow_individu = $this->lendingRepoGlobal->get_mtd_monthnow_individu();

        $mtd_monthsago_captive = $this->lendingRepoGlobal->get_mtd_monthsago_captive();
        $mtd_monthnow_captive = $this->lendingRepoGlobal->get_mtd_monthnow_captive();

        $mtd_monthsago_institusi = $this->lendingRepoGlobal->get_mtd_monthsago_institusi();
        $mtd_monthnow_institusi = $this->lendingRepoGlobal->get_mtd_monthnow_institusi();

        //data OS penyaluran dana persegment
        $os_monthsago_individu = $this->outstandingRepoGlobal->get_os_monthsago_individu();
        $os_monthnow_individu = $this->outstandingRepoGlobal->get_os_monthnow_individu();

        $os_monthsago_captive = $this->outstandingRepoGlobal->get_os_monthsago_captive();
        $os_monthnow_captive = $this->outstandingRepoGlobal->get_os_monthnow_captive();

        $os_monthsago_institusi = $this->outstandingRepoGlobal->get_os_monthsago_institusi();
        $os_monthnow_institusi = $this->outstandingRepoGlobal->get_os_monthnow_institusi();
        
        //data penyaluran dana
        $pd_monthsago = $this->penempatanRepoGlobal->get_pd_monthsago_individu();
        $pd_monthnow = $this->penempatanRepoGlobal->get_pd_monthnow_individu();
        $pd_target_monthnow = $this->penempatanRepoGlobal->get_target_pd_monthnow_individu();
        // dd($pd_monthsago);

        // text nominal hari ini
        $lending_now_individu = $this->lendingRepoGlobal->get_now_individu();
        $lending_now_captive = $this->lendingRepoGlobal->get_now_captive();
        $lending_now_institusi = $this->lendingRepoGlobal->get_now_institusi();
        $deposit_now_individu = $this->penempatanRepoGlobal->get_now_individu();
        $deposit_now_sum = $this->penempatanRepoGlobal->get_now_sum();
        $pendana_now = $this->lenderRepoGlobal->get_sumpendana_now();
        $pendana_now_aktif = $this->lenderRepoGlobal->get_sumpendana_now_aktif();
        $withdraw_now_sum = $this->withdrawRepoGlobal->get_now_sum();
        // dd($lending_now_individu);

// dd($lending_now_individu,$lending_now_captive,$lending_now_institusi);

        //data OS pendanaan aktif
        // $os_ytd_2yearsago = $this->outstandingRepoGlobal->get_os_ytd_2yearsago();
        // $os_ytd_1yearsago = $this->outstandingRepoGlobal->get_os_ytd_1yearsago();
        // $os_ytd_thisyear = $this->outstandingRepoGlobal->get_os_ytd_thisyear();

        $os_array =array(
            'os_monthsago_individu'=>$this->nice_number($os_monthsago_individu),
            'os_monthnow_individu'=>$this->nice_number($os_monthnow_individu),
            'os_monthsago_captive'=>$this->nice_number($os_monthsago_captive),
            'os_monthnow_captive'=>$this->nice_number($os_monthnow_captive),
            'os_monthsago_institusi'=>$this->nice_number($os_monthsago_institusi),
            'os_monthnow_institusi'=>$this->nice_number($os_monthnow_institusi)
        );

        $lendingarray =array(
            'ytd_1monthsago'=>$this->nice_number($ytd_1monthsago),
            'ytd_monthnow'=>$this->nice_number($ytd_monthnow),
            'target_monthnow'=>$this->nice_number($target_monthnow),
            'mtd_monthsago_individu' =>$this->nice_number($mtd_monthsago_individu),
            'mtd_monthnow_individu' =>$this->nice_number($mtd_monthnow_individu),
            'mtd_monthsago_captive' =>$this->nice_number($mtd_monthsago_captive),
            'mtd_monthnow_captive' =>$this->nice_number($mtd_monthnow_captive),
            'mtd_monthsago_institusi' =>$this->nice_number($mtd_monthsago_institusi),
            'mtd_monthnow_institusi' =>$this->nice_number($mtd_monthnow_institusi)
        );

        $pd_array =array(
            'pd_monthsago'=>$this->nice_number($pd_monthsago),
            'pd_monthnow'=>$this->nice_number($pd_monthnow),
            'pd_target_monthnow'=>$this->nice_number($pd_target_monthnow)
        );

        $daily_array=array(
            'lending_now_individu'=>$lending_now_individu,
            'lending_now_captive'=>$lending_now_captive,
            'lending_now_institusi'=>$lending_now_institusi,
            'deposit_now_individu'=> $deposit_now_individu,
            'deposit_now_sum'=> $deposit_now_sum,
            'pendana_now'=> $pendana_now,
            'pendana_now_aktif' => $pendana_now_aktif,
            'withdraw_now_sum' => $withdraw_now_sum
        );

        // dd($daily_array);

        // dd($array);
        if (empty($ytd_1monthsago)||empty($ytd_monthnow)||empty($target_monthnow)) {
            $pesan="Data Account ga ketemu"; return $this->DataResponse($lendingarray,$pesan,404);
        }else{
            return view('databulanberjalan',[
                'lending'=>json_encode($lendingarray),
                'outstanding'=>json_encode($os_array),
                'penempatan'=>json_encode($pd_array),
                'daily_array'=>json_encode($daily_array),

            ]);
            // return json_encode($array);
        }
    }
    
    //CRUD
    public function create(){
    }

    //method post
    public function store(Request $request){
    }

    //method get
    public function show($id=''){
    }

    //method get
    public function edit($id){
    }

    //method put
    public function update($id, Request $request){
    }

    //method delete
    public function destroy($id){
    }

    // function tambahan ===================
    private function DataResponse($data=[],$message='',$httpresponse='201'){
        return response()->json([
            'response'=>$httpresponse,
            'message'=>$message,
            'data'=>$data
        ],$httpresponse);
    }

    function nice_number($n) {
        // first strip any formatting;
        if ($n==null) {
            return 0;
        } else {
            # code...
            $n = (0+str_replace(",", "", $n));
            if ($n > 0) return round(($n/1000000000), 2);
            elseif ($n == 0) return 0 ;
        }
        return number_format($n);
    }
}
