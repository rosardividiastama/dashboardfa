<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLender extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('lender', function (Blueprint $table) {
            $table->lender_id();
            $table->date('lender_tgl_daftar');
            $table->string('lender_nama');
            $table->string('lender_email')->unique();
            $table->date('lender_tgl_lahir');
            $table->string('lender_gender');
            $table->string('lender_provinsi');
            $table->enum('lender_status', ['aktif', 'nonaktif']);   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
